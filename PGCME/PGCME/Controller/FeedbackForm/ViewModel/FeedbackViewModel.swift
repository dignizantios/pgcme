//
//  FeedbackViewModel.swift
//  PGCME
//
//  Created by Jaydeep on 17/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation


class FeedbackViewModel {
    
    var strQuestionariedId = String()
    var arrFeedbackDetail = [feedbackFormDetail]()
    var dictFeedbackDetail = FeedbackFormModel()
    //MARK: - View life cycle
    
    init() {
       
    }
}

extension FeedbackViewModel{
    
    func getFeedbackForm(id:String,completion:@escaping(String,String) -> Void){
        
        var strFeedBackTime = String()
        if let strTime = Defaults.value(forKey: "feedback_time") as? String{
            strFeedBackTime = strTime
        } else {
            strFeedBackTime = "00:00:00"
        }
        
        let url = kBaseURL + kGetFeedbackForm
        let param = ["lang" : lang,
                     "timezone" : getCurrentTimeZone(),
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "questionnaires_id" : "\(id)",
                     "time_check": strFeedBackTime
                    ]
        
        CommonService().Service(url: url, param: param) { (respones) in
            
            if let json = respones.value
            {
                print("JSON : \(json)")
                
                if json["flag"].stringValue == strSuccessResponse
                {
                    if let dicData =  json.dictionaryObject , let feedbackDetail = FeedbackFormModel(JSON:dicData) {
                        self.arrFeedbackDetail = feedbackDetail.data
                        self.dictFeedbackDetail = feedbackDetail
                    }
                    completion("", "")
                }
                else if json["flag"].stringValue == strAccessDenied   {
                    completion(strAccessDenied, "")
                } else if json["flag"].stringValue == "3"{
                    completion(json["flag"].stringValue, json["msg"].stringValue)
                } else if json["flag"].stringValue == "-2" {
                    completion(json["flag"].stringValue, json["msg"].stringValue)
                } else {
                    completion(json["flag"].stringValue, json["msg"].stringValue)
                    makeToast(strMessage: json["msg"].stringValue)
                }
            }
            else
            {
                completion("-2","Server_not_responding_Please_try_again_later_key".localized)
//                makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
            }
        }
    }
    
    func submitFeedbackFormAnswer(dict:[String:Any],completion:@escaping(String,String) -> Void){
        
        let url = kBaseURL + kPostFeedbackQuestionAnswer
        
        CommonService().Service(url: url, param: dict) { (respones) in
            
            if let json = respones.value
            {
                print("JSON : \(json)")
                
                if json["flag"].stringValue == strSuccessResponse {
                    completion(json["msg"].stringValue,json["flag"].stringValue)
                    makeToast(strMessage: json["msg"].stringValue)
                } else if json["flag"].stringValue == strAccessDenied {
                    completion(json["msg"].stringValue,json["flag"].stringValue)
                } else if json["flag"].stringValue == "-2" {
                    completion(json["msg"].stringValue,json["flag"].stringValue)
                } else {
                    completion(json["msg"].stringValue,json["flag"].stringValue)
                    makeToast(strMessage: json["msg"].stringValue)
                }
            }
            else
            {
                completion("-2","Server_not_responding_Please_try_again_later_key".localized)
//                makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
            }
        }
    }
}

//
//  FeedbackTableCell.swift
//  PGCME
//
//  Created by Jaydeep on 13/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class FeedbackTableCell: UITableViewCell {
    
    //MARK:- Variable Declaration
    
    var handlerTextview:(String,Int,Int) -> Void = {_,_,_ in}
    
    //MARK:- Outlet Zone
    @IBOutlet weak var txtViewAnswer: UITextView!
    @IBOutlet weak var collectionViewAnswer: UICollectionView!
    @IBOutlet weak var heightOfCollectionView: NSLayoutConstraint!
    @IBOutlet weak var vwSeperator: UIView!
    @IBOutlet weak var txtViewComment: CustomTextview!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var vwComment: UIView!
    
    //MARK:- View Life Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        txtViewAnswer.textColor = UIColor.appThemeDarkBlueColor
        txtViewAnswer.font = themeFont(size: 15, fontname: .medium)
        
        txtViewComment.textColor = UIColor.appThemeDarkBlueColor
        txtViewComment.font = themeFont(size: 15, fontname: .medium)
        txtViewComment.delegate = self
        txtViewComment?.textContainerInset = UIEdgeInsets(top: 7, left: 5, bottom: 7, right: 5)
        
        lblPlaceholder.textColor = .appThemeGrayColor
        lblPlaceholder.font = themeFont(size: 15, fontname: .medium)
        lblPlaceholder.text = "Enter_here_key".localized
        
        collectionViewAnswer.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
    }
    
    override func draw(_ rect: CGRect) {
        self.contentView.layoutIfNeeded()
        self.contentView.layoutSubviews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK:- Overide Method
       
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UICollectionView {
            print("collectionViewAnswer contentSize:= \(collectionViewAnswer.contentSize.height)")
            self.contentView.layoutIfNeeded()
            self.contentView.layoutSubviews()
            self.heightOfCollectionView.constant = collectionViewAnswer.contentSize.height
        }
    }
}
//MARK:- UITextview Delegate
extension FeedbackTableCell:UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == "" {
            self.lblPlaceholder.isHidden = false
        } else {
            self.lblPlaceholder.isHidden = true
        }
        let section = Int(textView.accessibilityValue!)
        handlerTextview(textView.text,textView.tag,section!)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

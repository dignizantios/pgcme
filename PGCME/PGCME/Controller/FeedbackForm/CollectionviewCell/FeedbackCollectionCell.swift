//
//  FeedbackCollectionCell.swift
//  PGCME
//
//  Created by Jaydeep on 13/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class FeedbackCollectionCell: UICollectionViewCell {
    
    //MARK:- Outlet Zone
    @IBOutlet weak var btnSelectButtonOutlet: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    //MARK:- ViewLife Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func draw(_ rect: CGRect) {
        self.contentView.layoutIfNeeded()
        self.contentView.layoutSubviews()
    }

}

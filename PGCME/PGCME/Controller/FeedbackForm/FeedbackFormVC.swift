//
//  FeedbackFormVC.swift
//  PGCME
//
//  Created by Jaydeep on 13/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import SwiftyJSON

class FeedbackFormVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var feedbackModel = FeedbackViewModel()
    var strMessage = String()
    var lblTimeCounter = UILabel()
    var graceTimer : Timer?
    var timeInterval = Int()
    var totalTimeInterval = Int()
    var remainingTimer : Timer?
    var remainngTimeInterval = Int()
    
     //MARK: - Outlet
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet var vwFooter: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSubmit: CustomButton!
    @IBOutlet weak var txtComment: UITextView!
    
    //MARK:- ViewLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        APICaling()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if graceTimer != nil {
            graceTimer?.invalidate()
            graceTimer = nil
        }
        
        if remainingTimer != nil {
            remainingTimer?.invalidate()
            remainingTimer = nil
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: "Feedback_Form_key".localized)
    }
    
    override func viewDidLayoutSubviews() {
        self.tableView.reloadData()
    }
    
    func APICaling(){
        feedbackModel.getFeedbackForm(id: feedbackModel.strQuestionariedId) { (error,msg) in
            if error == strAccessDenied {
                self.logoutAPICalling()
            } else if error == "3" {
                self.showPopupMessage(msg: msg)
            } else if error == "-2" {
                self.showPopupMessage(msg: msg)
            } else {
                self.strMessage = error
                self.tableView.tableFooterView = self.vwFooter
                self.tableView.reloadData()
                if self.feedbackModel.dictFeedbackDetail.remainingTime != "" || self.feedbackModel.dictFeedbackDetail.remainingTime != "00:00:00"{
                    self.lblTimeCounter = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 20))
                    self.lblTimeCounter.textColor = .white
                    self.lblTimeCounter.font = themeFont(size: 15, fontname: .medium)
                    self.lblTimeCounter.textAlignment = .right
                    self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.lblTimeCounter)
                    self.getRemainingIntervalFromDate()
//                    self.getIntervalFromDate()
                } else {
                    self.getIntervalFromDate()
                }
                
                if self.feedbackModel.dictFeedbackDetail.remainingFlag == "1" {
                    //self.getRemainingIntervalFromDate()
                }
            }
            
        }
    }
    
    func getIntervalFromDate(){
        let strTime = self.feedbackModel.dictFeedbackDetail.graceTime
       
        let date = StringToDate(Formatter: enumDateFormatter.dateFormatter2.rawValue, strDate: strTime)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        
        timeInterval = (hour*60*60) + (minute*60) + second
        totalTimeInterval = timeInterval
        graceTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    func getRemainingIntervalFromDate(){
        let strTime = self.feedbackModel.dictFeedbackDetail.remainingTime
       
        let date = StringToDate(Formatter: enumDateFormatter.dateFormatter2.rawValue, strDate: strTime)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        
        remainngTimeInterval = (hour*60*60) + (minute*60) + second
//        totalTimeInterval = remainngTimeInterval
        remainingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateRemainingTimer), userInfo: nil, repeats: true)
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    @objc func updateTimer() {
        if timeInterval <= 0{
           if graceTimer != nil {
                graceTimer?.invalidate()
                graceTimer = nil
//                self.submitAnswer()
            }
        } else {
            timeInterval -= 1
            print("Grace Timer = \(timeString(time: TimeInterval(timeInterval)))")
//            self.lblTimeCounter.text = "\(timeString(time: TimeInterval(timeInterval)))"
            Defaults.setValue(timeString(time: TimeInterval(totalTimeInterval - timeInterval)), forKey: "feedback_time")
            Defaults.synchronize()
        }
    }
    
    @objc func updateRemainingTimer() {
        if remainngTimeInterval <= 0{
            if remainingTimer != nil {
                remainingTimer?.invalidate()
                remainingTimer = nil
                getIntervalFromDate()
            }
            Defaults.setValue("00:00:00", forKey: "feedback_time")
            self.lblTimeCounter.text = ""
//            showPopupMessage(msg:"Sorry_the_allotted_time_exceeds_key".localized)
        } else {
            remainngTimeInterval -= 1
            self.lblTimeCounter.text = "\(timeString(time: TimeInterval(remainngTimeInterval)))"
            print("Remaining Timer = \(timeString(time: TimeInterval(remainngTimeInterval)))")
        }
    }
    
    func showPopupMessage(msg:String){
        let alertController = UIAlertController(title: "PG_CME_key".localized, message:msg , preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in

           let obj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
           self.navigateToSideMenuSelectedController(obj: obj)
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    /*override func backButtonTapped() {
        
        let alertController = UIAlertController(title: "", message: "Are_you_sure_you_want_to_quite_questionary?_key".localized, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Yes_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in

            self.submitAnswer()
            
        }
            
        let cancelAction = UIAlertAction(title: "No_key".localized, style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }*/

    func setupUI(){
        
        self.view.backgroundColor = UIColor.appThemeSkyBlueColor
        
        self.tableView.register(UINib(nibName: "QuestionaryHeaderCell", bundle: nil), forCellReuseIdentifier: "QuestionaryHeaderCell")
        self.tableView.tableFooterView = UIView()
        
        [txtComment].forEach { (txtView) in
            txtView?.font = themeFont(size: 17, fontname: .medium)
            txtView?.textColor = .appThemeDarkBlueColor
            txtView?.layer.cornerRadius = 5
            txtView?.clipsToBounds = true
            txtView?.layer.borderColor = UIColor.appThemeDarkBlueColor.cgColor
            txtView?.layer.borderWidth = 1
            txtView?.delegate = self
            txtView?.textContainerInset = UIEdgeInsets(top: 7, left: 5, bottom: 7, right: 5)
        }
        
        lblPlaceholder.textColor = .appThemeGrayColor
        lblPlaceholder.font = themeFont(size: 17, fontname: .medium)
        lblPlaceholder.text = "Additional_Suggestions_key".localized
        
        btnSubmit.setTitle("Submit_key".localized, for: .normal)
        btnSubmit.setupThemeButtonUI()
        
    }
    
    func setupData(id:String){
        feedbackModel.strQuestionariedId = id
    }

}

//MARK: - IBAction method

extension FeedbackFormVC{
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        if remainngTimeInterval <= 0 && timeInterval <= 0 {
            showPopupMessage(msg: "Sorry_the_allotted_time_exceeds_key".localized)
            return
        }
        
        var isOneAnswerSelected = true
        for i in 0..<feedbackModel.arrFeedbackDetail.count {
            if feedbackModel.arrFeedbackDetail[i].answerType == "2" {
                if feedbackModel.arrFeedbackDetail[i].answerText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
                    isOneAnswerSelected = false
                    break
                }
            } else {
                let arrOption = feedbackModel.arrFeedbackDetail[i].options
                for j in 0..<arrOption.count {
                    let dictInner = arrOption[j]
                    if dictInner.isSelected == false{
                        isOneAnswerSelected = false
                        break
                    }
                    /*for k in 0..<dictInner.subOptions.count{
                        let dictSuboption = dictInner.subOptions[k]
                        if dictSuboption.isSelected == false{
                            isOneAnswerSelected = false
                            break
                        }
                    }*/
                }
            }
        }
        
        if isOneAnswerSelected == false{
            makeToast(strMessage: "Please_select_or_enter_answer_of_all_questions_key".localized)
        } else {
            submitAnswer()
        }
    }
    
    func submitAnswer(){
        var dict:[String:Any] = [:]
        dict["user_id"] = getUserDetail("user_id")
        dict["access_token"] = getUserDetail("access_token")
        dict["timezone"] = getCurrentTimeZone()
        dict["lang"] = lang
        dict["questionnaires_id"] = feedbackModel.strQuestionariedId
//        dict["time_duration"] = "\(self.lblTimeCounter.text ?? "00:00:00")"
//        dict["fillup_time"] = "\(timeString(time: TimeInterval(totalTimeInterval - timeInterval)))"
        dict["answer_json"] = JSON(createAnswerJson()).rawString()
        dict["comment"] = self.txtComment.text
        print("dict\(dict)")
        
        feedbackModel.submitFeedbackFormAnswer(dict: dict) { (msg,flg) in
            if flg == strAccessDenied {
                self.logoutAPICalling()
            } else if flg == "-2" {
                self.showPopupMessage(msg: msg)
            } else {
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigateToSideMenuSelectedController(obj: obj)
            }
        }
    }
    
    func createAnswerJson() ->  [JSON] {
        var arrAnswerJson:[JSON] = []
        for i in 0..<feedbackModel.arrFeedbackDetail.count {
             var dict = JSON()
            if feedbackModel.arrFeedbackDetail[i].answerType == "2" {
                dict["questions_id"] = JSON(feedbackModel.arrFeedbackDetail[i].questionsId)
                dict["options_id"] = ""
                dict["answer"] = JSON(feedbackModel.arrFeedbackDetail[i].answerText.trimmingCharacters(in: .whitespaces))
                dict["answer_type"] = JSON(feedbackModel.arrFeedbackDetail[i].answerType)
                arrAnswerJson.append(dict)
            }
            else if feedbackModel.arrFeedbackDetail[i].answerType == "3" {
                dict["questions_id"] = JSON(feedbackModel.arrFeedbackDetail[i].questionsId)
                dict["options_id"] = ""
                dict["answer"] = JSON(feedbackModel.arrFeedbackDetail[i].answerText)
                dict["answer_type"] = JSON(feedbackModel.arrFeedbackDetail[i].answerType)
                arrAnswerJson.append(dict)
            }
            else {
                let arrOption = feedbackModel.arrFeedbackDetail[i].options
                for j in 0..<arrOption.count {
                    dict["questions_id"] = JSON(feedbackModel.arrFeedbackDetail[i].questionsId)
                    dict["options_id"] = ""
                    dict["answer"] = ""
                    dict["answer_type"] = JSON(feedbackModel.arrFeedbackDetail[i].answerType)
                    let dictInner = arrOption[j]
                    dict["options_id"].stringValue = dictInner.optionsId
                    for k in 0..<dictInner.subOptions.count{
                        let dictSuboption = dictInner.subOptions[k]
                        if dictSuboption.isSelected{
                            dict["answer"] = JSON(dictSuboption.selectedIndex)
                        }
                    }
                    arrAnswerJson.append(dict)
                }
            }
        }
        print("arrAnswerJson \(arrAnswerJson)")
        return arrAnswerJson
    }

}

//MARK:- Textview Delegate

extension FeedbackFormVC:UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        lblPlaceholder.isHidden = textView.text.isEmpty ? false : true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
}

//MARK:-TableView Delegate

extension FeedbackFormVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView{
            if feedbackModel.arrFeedbackDetail.count == 0{
                let lbl = UILabel()
                lbl.text = self.strMessage
                lbl.font = themeFont(size: 19, fontname: .bold)
                lbl.textAlignment = NSTextAlignment.center
                lbl.textColor = UIColor.appThemeGrayColor.withAlphaComponent(0.5)
                lbl.center = tableView.center
                tableView.backgroundView = lbl
                return 0
            }
            return feedbackModel.arrFeedbackDetail.count
        }
        if feedbackModel.arrFeedbackDetail[tableView.tag].answerType == "2" {
            return 1
        }
        return feedbackModel.arrFeedbackDetail[tableView.tag].options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionaryHeaderCell") as! QuestionaryHeaderCell
            cell.lblIndex.text = "\(indexPath.row + 1)."
            
            let dict = feedbackModel.arrFeedbackDetail[indexPath.row]
            
            if dict.answerType == "3" {
                cell.vwSlider.isHidden = false
                cell.vwSliderHeight.constant = 95
                cell.hanlorSliderValue = { value in
                    dict.answerText = "\(value)"
                }
            }
            
            cell.txtViewQuestion.attributedText = dict.questionName.htmlToAttributedString
            cell.tblSubData.register(UINib(nibName: "FeedbackTableCell", bundle: nil), forCellReuseIdentifier: "FeedbackTableCell")
            cell.tblSubData.tag = indexPath.row
            cell.tblSubData.delegate = self
            cell.tblSubData.dataSource = self
            cell.tblSubData.reloadData()
            cell.tblSubData.layoutIfNeeded()
            cell.tblSubData.layoutSubviews()
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackTableCell") as! FeedbackTableCell
        let arrQuestion = feedbackModel.arrFeedbackDetail[tableView.tag].options
        if indexPath.row == arrQuestion.count - 1 {
            cell.vwSeperator.isHidden = true
        } else {
            cell.vwSeperator.isHidden = false
        }
        
        print("tableView.tag \(tableView.tag)")
        print("AnswerType \(feedbackModel.arrFeedbackDetail[tableView.tag].answerType)")
        if feedbackModel.arrFeedbackDetail[tableView.tag].answerType == "2"{
            cell.vwComment.isHidden = false
            cell.txtViewComment.isHidden = false
            cell.collectionViewAnswer.isHidden = true
            cell.txtViewAnswer.isHidden = true
            cell.txtViewAnswer.text = ""
            cell.txtViewComment.accessibilityValue = "\(tableView.tag)"
            cell.txtViewComment.tag = indexPath.row
            cell.handlerTextview = {[weak self] text,tag,section in
                self?.feedbackModel.arrFeedbackDetail[section].answerText = text
            }
        } else {
            cell.vwComment.isHidden = true
            cell.txtViewAnswer.isHidden = false
            let dict = arrQuestion[indexPath.row]
            cell.txtViewAnswer.attributedText = dict.optionsName.htmlToAttributedString
            cell.collectionViewAnswer.isHidden = false
            cell.txtViewComment.isHidden = true
            cell.collectionViewAnswer.register(UINib(nibName: "FeedbackCollectionCell", bundle: nil), forCellWithReuseIdentifier: "FeedbackCollectionCell")
            cell.collectionViewAnswer.accessibilityValue = "\(tableView.tag)"
            cell.collectionViewAnswer.tag = indexPath.row
            cell.collectionViewAnswer.delegate = self
            cell.collectionViewAnswer.dataSource = self
            cell.collectionViewAnswer.reloadData()
            cell.collectionViewAnswer.layoutIfNeeded()
            cell.collectionViewAnswer.layoutSubviews()
        }
        return cell
    }
}

extension FeedbackFormVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedbackModel.arrFeedbackDetail[Int(collectionView.accessibilityValue!)!].options[collectionView.tag].subOptions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedbackCollectionCell", for: indexPath) as! FeedbackCollectionCell
        let arrSubOption = feedbackModel.arrFeedbackDetail[Int(collectionView.accessibilityValue!)!].options[collectionView.tag].subOptions
        let dict = arrSubOption[indexPath.row]
        cell.lblTitle.text = dict.name
        if dict.isSelected {
            cell.btnSelectButtonOutlet.isSelected = true
        } else {
            cell.btnSelectButtonOutlet.isSelected = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width-10)/2, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var arrSubOption = feedbackModel.arrFeedbackDetail[Int(collectionView.accessibilityValue!)!].options[collectionView.tag].subOptions
        for i in 0..<arrSubOption.count{
            let dict = arrSubOption[i]
            dict.isSelected = false
            dict.selectedIndex = 0
            arrSubOption[i] = dict
        }
        
        feedbackModel.arrFeedbackDetail[Int(collectionView.accessibilityValue!)!].options[collectionView.tag].subOptions = arrSubOption
        
        feedbackModel.arrFeedbackDetail[Int(collectionView.accessibilityValue!)!].answerText = "\(indexPath.row+1)"
        feedbackModel.arrFeedbackDetail[Int(collectionView.accessibilityValue!)!].options[collectionView.tag].isSelected = true
        arrSubOption = feedbackModel.arrFeedbackDetail[Int(collectionView.accessibilityValue!)!].options[collectionView.tag].subOptions
        let dict = arrSubOption[indexPath.row]
        dict.isSelected = true
        dict.selectedIndex = indexPath.row+1
        arrSubOption[indexPath.row] = dict
        
        feedbackModel.arrFeedbackDetail[Int(collectionView.accessibilityValue!)!].options[collectionView.tag].subOptions = arrSubOption
       
        
        UIView.performWithoutAnimation {
            self.tableView.reloadData()
        }
    }
}

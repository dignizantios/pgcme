import Foundation 
import ObjectMapper 

class FeedbackFormModel: Mappable { 

	var finishDatetime : String = ""
	var flag: Int = 0
	var isQuestionaries : String = ""
	var graceTime : String = ""
	var startDatetime : String = ""
	var questionnairesId : String = ""
	var msg : String = ""
    var remainingTime:String = ""
    var remainingFlag:String = ""
	var data = [feedbackFormDetail]()

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		finishDatetime <- map["finish_datetime"] 
		flag <- map["flag"] 
		isQuestionaries <- map["is_questionaries"] 
		graceTime <- map["grace_time"] 
		startDatetime <- map["start_datetime"] 
		questionnairesId <- map["questionnaires_id"] 
		msg <- map["msg"]
        remainingTime <- map["remaining_time"]
        remainingFlag <- map["remaining_flag"]
		data <- map["data"] 
	}
} 

class feedbackFormDetail: Mappable {

	var options = [Options]()
	var questionsId : String = ""
	var questionName : String = ""
    var answerType:String = ""
    var answerText:String = ""

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		options <- map["options"] 
		questionsId <- map["questions_id"] 
		questionName <- map["question_name"]
        answerType <- map["answer_type"]
	}
} 

class Options: Mappable { 

	var subOptions = [SubOptions]()
	var optionsName : String = ""
	var optionsId : String = ""
    var isSelected : Bool = false
    var selectedIndex:Int = 0

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		subOptions <- map["sub_options"] 
		optionsName <- map["options_name"] 
		optionsId <- map["options_id"] 
	}
} 

class SubOptions: Mappable { 

	var name : String = ""
	var id : String = ""
    var isSelected : Bool = false
    var selectedIndex:Int = 0

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		name <- map["name"] 
		id <- map["id"] 
	}
} 


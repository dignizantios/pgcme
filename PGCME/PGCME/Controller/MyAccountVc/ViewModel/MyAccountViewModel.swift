//
//  MyAccountViewModel.swift
//  PGCME
//
//  Created by YASH on 16/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation

class MyAccountViewModel{
    
    var dictMyAccountData = myAccountDetail()
    
    //MARK: - View life cycle
    
    init() {
       
    }
    
}

extension MyAccountViewModel{
    
    func getProfileDetails(completion:@escaping(String) -> Void){
        
        let url = kBaseURL + kGetAndUpdateProfileDetails
        let param = ["lang" : lang,
                     "timezone" : getCurrentTimeZone(),
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "flag_view" : "0"
                    ]
        
        CommonService().Service(url: url, param: param) { (respones) in
            
            if let json = respones.value
            {
                print("JSON : \(json)")
                
                if json["flag"].stringValue == strSuccessResponse
                {
                    if let dicData =  json.dictionaryObject , let myAccountDetail = MyAccountModel(JSON:dicData) {
                        self.dictMyAccountData = myAccountDetail.myAccountData
                    }
                    completion("")
                }
                else if json["flag"].stringValue == strAccessDenied
                {
                    completion(strAccessDenied)
                }
                else
                {
                    makeToast(strMessage: json["msg"].stringValue)
                }
            }
            else
            {
                makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
            }
        }
    }
}


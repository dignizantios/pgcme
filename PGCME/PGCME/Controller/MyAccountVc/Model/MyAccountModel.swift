//
//  MyAccountModel.swift
//  PGCME
//
//  Created by Jaydeep on 16/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import ObjectMapper

class MyAccountModel: Mappable {

    var myAccountData =  myAccountDetail()

    required convenience init?(map: Map){
        self.init()
    }

    func mapping(map: Map) {
        myAccountData <- map["data"]
    }
}

class myAccountDetail: Mappable {

    var registerNo : String = ""
    var bonafide: String = ""
    var state: String = ""
    var emailId: String = ""
    var userId: String = ""
    var age: String = ""
    var name: String = ""
    var userName: String = ""
    var registerFee: String = ""
    var course: String = ""
    var yearStudy: String = ""
    var nameChangeValid: String = ""
    var city: String = ""
    var pincode: String = ""
    var address: String = ""
    var mobileNo: String = ""
    var institution: String = ""
    var nameOfInstitute: String = ""
    var telephone: String = ""
    var paidStatus: String = ""
    var accessToken: String = ""
    var sex: String = ""
    var paymentFlag: String = ""
    var dob: String = ""
    var transactionDate:String = ""
    var transactionNumber:String = ""
    var receiptPdf:String = ""
    var isInstituteFlag : String = ""
    var studyOther = ""
    var courseOther = ""
    var credit = ""
    

    required convenience init?(map: Map){
        self.init()
    }

    func mapping(map: Map) {
        registerNo <- map["reg_no"]
        bonafide <- map["bonafide"]
        state <- map["state"]
        emailId <- map["email_id"]
        userId <- map["user_id"]
        age <- map["age"]
        name <- map["name"]
        userName <- map["user_name"]
        registerFee <- map["register_fee"]
        course <- map["course"]
        yearStudy <- map["year_study"]
        nameChangeValid <- map["name_change_valid"]
        city <- map["city"]
        pincode <- map["pincode"]
        address <- map["address"]
        mobileNo <- map["mobile_no"]
        institution <- map["institution"]
        telephone <- map["telephone"]
        paidStatus <- map["paid_status"]
        accessToken <- map["access_token"]
        sex <- map["sex"]
        paymentFlag <- map["payment_flag"]
        dob <- map["dob"]
        transactionDate <- map["transaction_date"]
        transactionNumber <- map["transaction_number"]
        receiptPdf <- map["receipt_pdf"]
        nameOfInstitute <- map["name_of_institution"]
        isInstituteFlag <- map["is_institute_flag"]
        studyOther <- map["study_other"]
        courseOther <- map["course_other"]
        credit <- map["credit"]
    }
}


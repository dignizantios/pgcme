//
//  MyAccountVc.swift
//  PGCME
//
//  Created by YASH on 25/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SDWebImage

class MyAccountVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var btnProfile: CustomButton!
    @IBOutlet weak var lblRegisterNumber: UILabel!
    @IBOutlet weak var lblRegisterNumberValue: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNameValue: UILabel!
    @IBOutlet weak var lblSex: UILabel!
    @IBOutlet weak var lblSexValue: UILabel!
    @IBOutlet weak var lblInstitution: UILabel!
    @IBOutlet weak var lblInstitutionValue: UILabel!
    @IBOutlet weak var lblNameOfInstitution: UILabel!
    @IBOutlet weak var lblNameOfInstitutionValue: UILabel!
    @IBOutlet weak var lblCourse: UILabel!
    @IBOutlet weak var lblCourseValue: UILabel!
    @IBOutlet weak var lblYearOfStudy: UILabel!
    @IBOutlet weak var lblYearOfStudyValue: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserNameValue: UILabel!
    @IBOutlet weak var lblEmailID: UILabel!
    @IBOutlet weak var lblEmailIDValue: UILabel!
    @IBOutlet weak var lblTelephoneNo: UILabel!
    @IBOutlet weak var lblTelephoneNoValue: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblMobileNoValue: UILabel!
    @IBOutlet weak var lblDateOfBirth: UILabel!
    @IBOutlet weak var lblDateofBirthValue: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblAgeValue: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblAddressValue: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblCityValue: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblStateValue: UILabel!
    @IBOutlet weak var lblPincode: UILabel!
    @IBOutlet weak var lblPincodeValue: UILabel!
    @IBOutlet weak var lblBonafideCertificate: UILabel!
    @IBOutlet weak var imgBonafideCertificate: UIImageView!
    @IBOutlet weak var btnReceipt: CustomButton!
    @IBOutlet weak var lblTransactionDate: UILabel!
    @IBOutlet weak var lblTransactionDateValue: UILabel!
    @IBOutlet weak var lblTransactionNumber: UILabel!
    @IBOutlet weak var lblTransactionNumberValue: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblAmountValue: UILabel!
    @IBOutlet weak var btnChangePassword: CustomButton!
    @IBOutlet weak var btnDownload: CustomButton!
    @IBOutlet weak var btnPayOnline: CustomButton!
    @IBOutlet weak var lblRegisterFeeTitle: UILabel!
    @IBOutlet weak var viewReceipt: UIView!
    @IBOutlet weak var viewPayonline: UIView!
    @IBOutlet weak var lblRegisterFeeValue: UILabel!
    @IBOutlet weak var btnNetBankingOutlet: UIButton!
    @IBOutlet weak var btnCreditCardOutlet: UIButton!
    @IBOutlet weak var btnPayNowOutlet: CustomButton!
    
    ///--Vp
    @IBOutlet weak var vwSpecifyCource: UIView!
    @IBOutlet weak var lblTitleSpecifyCource: UILabel!
    @IBOutlet weak var lblValueSpecifyCource: UILabel!
    
    @IBOutlet weak var vwSpecifyYear: UIView!
    @IBOutlet weak var lblTitleSpecifyYear: UILabel!
    @IBOutlet weak var lblValueSpecifyYear: UILabel!
    
    @IBOutlet weak var vwCmeCertificate: UIView!
    @IBOutlet weak var lblTitleCmeCertificate: UILabel!
    @IBOutlet weak var lblValueCmeCertificate: UILabel!
    @IBOutlet weak var vwCertificate: CustomView!
    ///--
    
    //MARK: - Variable
    
    var viewModel = MyAccountViewModel()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        APICalling()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithSideMenuButton(titleText: "My_Account_key".localized)
    }
    
    override func viewDidLayoutSubviews() {
        [btnCreditCardOutlet,btnNetBankingOutlet].forEach { (btn) in
            
        }
    }
    
    func APICalling(){
        viewModel.getProfileDetails(completion: {msg in
            if msg == strAccessDenied {
                self.logoutAPICalling()
            } else {
              self.setupData(theModel: self.viewModel.dictMyAccountData)
            }
        })
    }
    
    func setupUI(){
        self.view.backgroundColor = UIColor.appThemeSkyBlueColor
        
        lblBonafideCertificate.text = "Bonafide_Certificate_key".localized;
        lblBonafideCertificate.textColor = UIColor.appThemeDarkBlueColor
        lblBonafideCertificate.font = themeFont(size: 20, fontname: .medium)
        
        [btnProfile,btnPayOnline,btnReceipt].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 26, fontname: .bold)
            btn?.setTitleColor(UIColor.appThemeDarkBlueColor, for: .normal)
        }
        
        btnProfile.setTitle("Profile_key".localized, for: .normal)
        btnReceipt.setTitle("Receipt_key".localized, for: .normal)
        btnPayOnline.setTitle("Pay_Online_key".localized, for: .normal)
        
        lblRegisterNumber.text = "Registration_Number_key".localized
        lblName.text = "Name_key".localized
        lblSex.text = "Sex_key".localized
        lblDateOfBirth.text = "Date_of_Birth_key".localized
        lblInstitution.text = "Institution_key".localized
        lblCourse.text = "Course_key".localized
        lblYearOfStudy.text = "Year_of_Study_key".localized
        lblAddress.text = "Address_key".localized
        lblCity.text = "City_key".localized
        lblState.text = "State_key".localized
        lblPincode.text = "Pincode_key".localized
        lblUserName.text = "Username_key".localized
        lblEmailID.text = "Email_ID_key".localized
        lblTelephoneNo.text = "Telephone_No_key".localized
        lblMobileNo.text = "Mobile_No_key".localized
        lblAge.text = "Age_key".localized
        lblTransactionDate.text = "Transaction_Date_key".localized
        lblTransactionNumber.text = "Transaction_Number_key".localized
        lblAmount.text = "Amount_key".localized
        lblNameOfInstitution.text = "Name_of_Institution_key".localized
        lblRegisterFeeTitle.text = "Register_fee_key".localized.capitalized
        [lblRegisterNumber,lblName,lblSex,lblDateOfBirth,lblInstitution,lblCourse,lblYearOfStudy,lblAddress,lblCity,lblState,lblPincode,lblUserName,lblEmailID,lblTelephoneNo,lblMobileNo,lblAge,lblTransactionDate,lblTransactionNumber,lblAmount,lblRegisterFeeTitle, lblNameOfInstitution, lblTitleSpecifyYear, lblTitleSpecifyCource, lblTitleCmeCertificate].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeGrayColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        [lblRegisterNumberValue,lblNameValue,lblSexValue,lblDateofBirthValue,lblInstitutionValue,lblCourseValue,lblYearOfStudyValue,lblAddressValue,lblCityValue,lblStateValue,lblPincodeValue,lblUserNameValue,lblEmailIDValue,lblTelephoneNoValue,lblMobileNoValue,lblAgeValue,lblTransactionDateValue,lblTransactionNumberValue,lblAmountValue, lblNameOfInstitutionValue, lblValueSpecifyYear, lblValueSpecifyCource, lblValueCmeCertificate].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeGrayColor
            lbl?.font = themeFont(size: 14, fontname: .light)
            lbl?.text = ""
        }
        
        lblRegisterFeeValue.textColor = UIColor.appThemeGreenColor
        lblRegisterFeeValue.font = themeFont(size: 15, fontname: .medium)
        
        btnChangePassword.setupThemeButtonUI()
        btnDownload.setupThemeButtonUI()
        btnPayNowOutlet.setupThemeButtonUI()
        
        btnPayNowOutlet.setTitle("Pay_now_key".localized, for: .normal)
        btnChangePassword.setTitle("Change_Password_key".localized, for: .normal)
        btnDownload.setTitle("Download_key".localized, for: .normal)
        
        imgBonafideCertificate.layer.cornerRadius = 8.0
        imgBonafideCertificate.layer.masksToBounds = true
        
        [btnNetBankingOutlet,btnCreditCardOutlet].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .medium)
            btn?.setTitleColor(.appThemeGrayColor, for: .normal)
            btn?.setTitleColor(.appThemeGrayColor, for: .selected)
        }
        
        btnNetBankingOutlet.setTitle("Net_banking_key".localized.capitalized, for: .normal)
        btnCreditCardOutlet.setTitle("Credit_card_key".localized.capitalized, for: .normal)
        
        ///--vp
        [vwCmeCertificate, vwSpecifyYear, vwSpecifyCource].forEach { (vw) in
            vw?.isHidden = true
        }
        ///--
        
    }
    
    func setupData(theModel:myAccountDetail){
        self.lblRegisterNumberValue.text = theModel.registerNo
        self.lblNameValue.text = theModel.name
        self.lblSexValue.text = theModel.sex
        self.lblInstitutionValue.text = theModel.institution
        self.lblNameOfInstitutionValue.text = theModel.nameOfInstitute
        self.lblCourseValue.text = theModel.course
        self.lblYearOfStudyValue.text = theModel.yearStudy
        self.lblUserNameValue.text = theModel.userName
        self.lblEmailIDValue.text = theModel.emailId
        self.lblTelephoneNoValue.text = theModel.telephone
        self.lblMobileNoValue.text = theModel.mobileNo
        self.lblDateofBirthValue.text = theModel.dob
        self.lblAgeValue.text = theModel.age
        self.lblAddressValue.text = theModel.address
        self.lblCityValue.text = theModel.city
        self.lblStateValue.text = theModel.state
        self.lblPincodeValue.text = theModel.pincode
        if theModel.bonafide.contains(".pdf") {
            self.imgBonafideCertificate.image = UIImage(named: "ic_pdf")
        } else {
            self.imgBonafideCertificate.sd_setImage(with: URL(string: theModel.bonafide), placeholderImage: UIImage(named: "ic_bonafide _certificate"), options: .lowPriority, completed: nil)
        }
        self.lblAmountValue.text = "₹ " + theModel.registerFee
        self.lblTransactionDateValue.text = theModel.transactionDate
        self.lblTransactionNumberValue.text = theModel.transactionNumber
        
        if theModel.paymentFlag == "1"{
            viewReceipt.isHidden = false
            viewPayonline.isHidden = true
        }
        else if theModel.isInstituteFlag == "0" {
            viewReceipt.isHidden = true
            viewPayonline.isHidden = true
        }
        else {
            viewReceipt.isHidden = true
            viewPayonline.isHidden = false
            lblRegisterFeeValue.text = "₹ " + theModel.registerFee
        }
        
        ///--vp
        [vwSpecifyYear, vwSpecifyCource].forEach { (vw) in
            vw?.isHidden = true
        }
        if theModel.course == "Other" {
            vwSpecifyCource.isHidden = false
            self.lblValueSpecifyCource.text = theModel.courseOther
        }
        if theModel.yearStudy == "Other" {
            vwSpecifyYear.isHidden = false
            self.lblValueSpecifyYear.text = theModel.studyOther
        }
        if theModel.institution == "Other" {
            vwCmeCertificate.isHidden = false
            self.lblValueCmeCertificate.text = theModel.credit
            
            if theModel.credit == "Not Required" {
                [vwCertificate, viewReceipt, viewPayonline].forEach { (vw) in
                    vw?.isHidden = true
                }
            }
            
        }
        ///--
        
    }
    
}

//MARK: - IBAction
extension MyAccountVc{
    
    @IBAction func btnEditProfileTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVc") as! EditProfileVc
        obj.setupModel(theModel: self.viewModel.dictMyAccountData)
        obj.handlerUpdateProfile = {[weak self] in
            self?.viewModel.getProfileDetails (completion: {msg in
                if msg == strAccessDenied {
                    NotificationCenter.default.post(name: .didLogoutUser, object: nil, userInfo: nil)
                } else {
                  self?.setupData(theModel: (self?.viewModel.dictMyAccountData)!)
                }
            })
        }
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnViewerAction(_ sender:UIButton){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WebDetailsVc") as! WebDetailsVc
        obj.selectedController = .certificate
        obj.strURL = self.viewModel.dictMyAccountData.bonafide
        obj.strTitle = "Bonafide_Certificate_key".localized
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnChangePasswordTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVc") as! ChangePasswordVc
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnNetBankingAction(_ sender:UIButton){
        sender.isSelected = true
        btnCreditCardOutlet.isSelected = false
    }
    
    @IBAction func btnCreditCardAction(_ sender:UIButton){
        sender.isSelected = true
        btnNetBankingOutlet.isSelected = false
    }
    
    @IBAction func btnPayNowAction(_ sender:UIButton){
        
        if btnCreditCardOutlet.isSelected == false && btnNetBankingOutlet.isSelected == false{
            makeToast(strMessage: "Please_select_payment_type".localized)
            return
        }
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
        obj.strPaymentType = btnCreditCardOutlet.isSelected == true ? "0" : "1"
        obj.handlerUpdatePayment = {[weak self] in
            makeToast(strMessage: "Thank you for the payment. Selection letter mail has been sent to your registered email id. Kindly check it.".localized)
            self?.APICalling()
            self?.checkSidemenuFlag {
                print("Called")
            }
        }
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnDownloadReceiptTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WebDetailsVc") as! WebDetailsVc
        obj.selectedController = .download
        obj.strURL = self.viewModel.dictMyAccountData.receiptPdf
        obj.strTitle = "Receipt_key".localized
        self.navigationController?.pushViewController(obj, animated: true)
        
        /*
        let receiptPdf = self.viewModel.dictMyAccountData.receiptPdf
        
        if receiptPdf == "" {
            makeToast(strMessage: "Receipt_not_found_key".localized)
            return
        }
        
        let destination = DownloadRequest.suggestedDownloadDestination()

        let pdfUrl = URL(string: receiptPdf)

        Alamofire.download(pdfUrl!, to: destination).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { (progress) in
            print("Progress: \(progress.fractionCompleted)")
            } .validate().responseData { ( response ) in
                print(response.destinationURL)
                print("Status code",response.response?.statusCode)
                makeToast(strMessage: "Downloaded")
        }
        */
    }
    
}

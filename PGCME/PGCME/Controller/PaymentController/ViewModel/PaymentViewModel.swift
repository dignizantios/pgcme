//
//  PaymentViewModel.swift
//  PGCME
//
//  Created by Jaydeep on 03/02/20.
//  Copyright © 2020 Yash. All rights reserved.
//

import Foundation

class PaymentViewModel {
}

extension PaymentViewModel{
    func getPaymentURL(p_type:String,completion:@escaping(Int,String) -> Void){
            
            let url = kBaseURL + kGetPayment
            let param = ["lang" : lang,
                         "timezone" : getCurrentTimeZone(),
                         "user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "payment_type":p_type
                        ]
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        if let dicData =  json.dictionaryObject , let paymentDetail = PaymentModel(JSON:dicData) {
                            completion(paymentDetail.flag,paymentDetail.paymentUrl)
                            return
                        }
                        completion(0,"")
                    } else if json["flag"].stringValue == strAccessDenied {
                        completion(json["flag"].intValue, "")
                    }  else {
//                        completion(json["flag"].intValue, json["msg"].stringValue)
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            }
        }
}


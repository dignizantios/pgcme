import Foundation 
import ObjectMapper 

class PaymentModel: Mappable { 

	var flag: Int = -1
	var msg: String = ""
	var paymentUrl: String = ""

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		paymentUrl <- map["payment_url"] 
	}
} 


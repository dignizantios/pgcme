//
//  PaymentVC.swift
//  PGCME
//
//  Created by Jaydeep on 03/02/20.
//  Copyright © 2020 Yash. All rights reserved.
//

import UIKit
import WebKit

class PaymentVC: UIViewController {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var webViewPayment: WKWebView!
    
    //MARK:- Variable Declaration
    
    var strPaymentType:String = ""
    var model = PaymentViewModel()
    var handlerUpdatePayment:() -> Void = {}
    
    //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        webViewPayment.navigationDelegate = self
        webViewPayment.uiDelegate = self
        model.getPaymentURL(p_type: strPaymentType) { (flg, url) in
            if String(flg) == strAccessDenied {
                self.logoutAPICalling()
            } else {
                self.webViewPayment.load(URLRequest(url: URL(string: url)!))
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: "Payment_key".localized)
    }
    
}

//MARK:- WkWEbview

extension PaymentVC : WKUIDelegate,WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showLoader()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideLoader()
    }
   
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        print("navigation URL \(navigationAction.request.url!)")
        
        if let urlStr = navigationAction.request.url?.absoluteString {
            print("navigationAction URL \(urlStr)")
            if urlStr.contains("payment_app_success.php?flag=1"){
                print("Got it")
                self.handlerUpdatePayment()
                self.navigationController?.popViewController(animated: true)
                decisionHandler(.cancel)
                return
            } else if urlStr.contains("payment_app_success.php?flag=0") {
                self.navigationController?.popViewController(animated: true)
                decisionHandler(.cancel)
                return
            }
        }
        decisionHandler(.allow)
    }
}

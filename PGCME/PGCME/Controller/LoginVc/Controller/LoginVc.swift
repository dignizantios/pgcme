//
//  LoginVc.swift
//  PGCME
//
//  Created by YASH on 23/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class LoginVc: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var txtUserName: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPassword: UILabel!

    @IBOutlet weak var btnSignIn: CustomButton!
    @IBOutlet weak var btnSignup: CustomButton!
    
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnEmailVerification: UIButton!
    
    @IBOutlet weak var btnLogin: CustomButton!
    @IBOutlet weak var constantTopWithImage: NSLayoutConstraint!
    @IBOutlet weak var stackBtn: UIStackView!
    
    //MARK: - Variable
    
    var viewModel = LoginViewModel()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.completionHandlerForSuccessLogin = { [weak self] in            
            let obj = self?.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self?.navigateToSideMenuSelectedController(obj: obj)
        }
        
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithSideMenuButton(titleText: "Login_key".localized)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    //MARK: - SetupUI
    
    func setupUI(){
        
        self.view.backgroundColor = UIColor.appThemeSkyBlueColor
        
        btnSignIn.titleLabel?.font = themeFont(size: 30, fontname: .bold)
        btnSignIn.setTitle("Sign_in_key".localized, for: .normal)
        btnSignIn.setTitleColor(UIColor.appThemeGrayColor, for: .normal)
        
        btnSignup.titleLabel?.font = themeFont(size: 17, fontname: .medium)
        btnSignup.setTitle("Sign_up_key".localized, for: .normal)
        btnSignup.setTitleColor(UIColor.appThemeDarkBlueColor, for: .normal)
        
        [lblUserName,lblPassword].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeGrayColor
            lbl?.font = themeFont(size: 14, fontname: .medium)
        }
        
        lblUserName.text = "Username_key".localized
        lblPassword.text = "Password_key".localized
        
        [txtUserName,txtPassword].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.textColor = UIColor.appThemeDarkBlueColor
            txt?.placeholder = "Enter_here_key".localized
        }
        
        [btnForgotPassword,btnEmailVerification].forEach { (btn) in
            btn?.setTitleColor(UIColor.appThemeGrayColor, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        }
        
        btnForgotPassword.setTitle("Forgot_password_?_key".localized, for: .normal)
        btnEmailVerification.setTitle("Email_Verification_key".localized, for: .normal)
        
        btnLogin.setTitle("Login_key".localized.uppercased(), for: .normal)
        btnLogin.setupThemeButtonUI()
        
        if UIScreen.main.bounds.width <= 320
        {
            self.constantTopWithImage.constant = 0
            self.stackBtn.spacing = 5
        }
        
    }
    
    
}

//MARK: - IBAction

extension LoginVc{
    
    @IBAction func btnLoginTapped(_ sender: UIButton) {
        
        if (txtUserName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: "Please_enter_username_key".localized)
        }
        else if (txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: "Please_enter_password_key".localized)
        }
        else
        {
            
            let param = ["user_name" : txtUserName.text ?? "",
                         "password" : txtPassword.text ?? "",
                         "lang": lang,
                         "timezone" : getCurrentTimeZone(),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                         "register_id" : "45",
                         "device_type" : strDeviceType]
            
            viewModel.loginServices(param: param)
        }
        
    }
    
    @IBAction func btnHideShowPasswordTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        txtPassword.isSecureTextEntry = sender.isSelected ? false : true
    }
    
    @IBAction func btnSignUpTapped(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVc") as! RegisterVc
        self.navigateToSideMenuSelectedController(obj: obj)
    }
    
    @IBAction func btnForgotPasswordTapped(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVc") as! ForgotPasswordVc
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnEmailVerificationTapped(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "EmailVerificationVc") as! EmailVerificationVc
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}

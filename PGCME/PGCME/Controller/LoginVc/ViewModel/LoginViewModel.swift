//
//  LoginViewModel.swift
//  PGCME
//
//  Created by YASH on 14/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation
import SwiftyJSON

class LoginViewModel{
    
    //MARK: - Variable
    
    var completionHandlerForSuccessLogin:() -> Void = {}
    
    
    //MARK:- Life Cycle
    init() {

    }
}

extension LoginViewModel{
    
    func loginServices(param:[String:Any]){
        
        let url = kBaseURL + kLogin
        
        CommonService().Service(url: url, param: param) { (response) in
            
            if let dictData = response.value{
                
                if dictData["flag"].stringValue == strSuccessResponse{
                    
                    let data = dictData["data"]
                    print("Login Data: \(data)")
                    guard let rowdata = try? data.rawData() else {return}
                    Defaults.setValue(rowdata, forKey: "userDetails")
                    Defaults.synchronize()
                    
                    self.completionHandlerForSuccessLogin()
                    
                }else if dictData["flag"].stringValue == strAccessDenied{
                   
                }else{
                    makeToast(strMessage: dictData["msg"].stringValue)
                }
            }
        }
        
    }
    
}

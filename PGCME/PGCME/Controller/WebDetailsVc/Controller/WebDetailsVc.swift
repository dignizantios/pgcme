//
//  WebDetailsVc.swift
//  PGCME
//
//  Created by YASH on 27/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import WebKit
import Alamofire

enum comeFromForWebDetails{
    case home
    case educationMaterial
    case certificate
    case download
}


class WebDetailsVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var webData: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: - Variable
    
    var selectedController = comeFromForWebDetails.home
    var strTitle = String()
    var strURL = ""
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var url : URL?
        
        if selectedController == .home{
            url = URL(string: strURL)!
        }else if selectedController == .educationMaterial{
            url = URL(string: strURL)!
        } else if selectedController == .certificate {
            url = URL(string: strURL)!
        }
        else if selectedController == .download {
            url = URL(string: strURL)!
            self.navigationRightBtnSetup()
        }
        webData.navigationDelegate = self
        webData.load(URLRequest(url: url!))
        webData.allowsBackForwardNavigationGestures = true
        
//        activityIndicator.color = UIColor.appThemeDarkBlueColor
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: strTitle)
    }

    func navigationRightBtnSetup() {
        
        let btnDownload = UIBarButtonItem(image: UIImage(named: "ic_white_download"), style: .plain, target: self, action: #selector(btnDownloadAction))
        btnDownload.tintColor = .white
        self.navigationItem.rightBarButtonItem = btnDownload
    }
    
    @objc func btnDownloadAction() {
        print("Download: ")
        
        let receiptPdf = strURL
        if receiptPdf == "" {
            makeToast(strMessage: "Receipt_not_found_key".localized)
            return
        }
        // Serial: FFNYF6ATJC6C · UDID: a3b6848dcf3d90b88326f6d003e486982fc942f1 · Model: iPhone10,1
        let destination = DownloadRequest.suggestedDownloadDestination()
        let pdfUrl = URL(string: receiptPdf)
        Alamofire.download(pdfUrl!, to: destination).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { (progress) in
            print("Progress: \(progress.fractionCompleted)")
            } .validate().responseData { ( response ) in
                print(response.destinationURL)
                print("Status code",response.response?.statusCode)
                makeToast(strMessage: "Downloaded")
        }
    }
}

extension WebDetailsVc: WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
//        activityIndicator.startAnimating()
        showLoader()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
//        activityIndicator.stopAnimating()
        hideLoader()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    
}

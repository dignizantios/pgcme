//
//  AnnouncementVc.swift
//  PGCME
//
//  Created by YASH on 26/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class AnnouncementVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variable
    
    var viewModel = AnnouncementViewModel()
    var strErrorMsg = ""
    var upperReferesh = UIRefreshControl()

    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        viewModel.completionHandlerSuccess = {[weak self] in
            self?.upperReferesh.endRefreshing()
            self?.tableView.reloadData()
        }
        
        viewModel.completionHandlerFailure = {[weak self] (errorMsg) in
            if errorMsg == strAccessDenied {
                self?.logoutAPICalling()
            } else {
                self?.upperReferesh.endRefreshing()
                self?.strErrorMsg = errorMsg
                self?.tableView.reloadData()
            }
            
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithSideMenuButton(titleText: "Announcements_key".localized)
    }
    
    func setupUI(){
        
        self.view.backgroundColor = UIColor.appThemeSkyBlueColor
        
        self.tableView.register(UINib(nibName: "AnnounementTblCell", bundle: nil), forCellReuseIdentifier: "AnnounementTblCell")
        
        self.tableView.tableFooterView = UIView()
        
        upperReferesh.tintColor = UIColor.appThemeDarkBlueColor
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        
        tableView.addSubview(upperReferesh)
    }
    
    @objc func upperRefreshTable(){
        viewModel.getAnnouncementList(isShowLoader:false)
    }

}


//MARK: - TableView Delegate

extension AnnouncementVc: UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if viewModel.arrayAnnouncement.count == 0{
            
            let lbl = UILabel()
            lbl.text = self.strErrorMsg
            lbl.font = themeFont(size: 16, fontname: .medium)
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeGrayColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        
        return viewModel.arrayAnnouncement.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnnounementTblCell") as! AnnounementTblCell
        
        let dict = viewModel.arrayAnnouncement[indexPath.row]
        
        cell.lblTitle.text = dict.title
        cell.lblDate.text = dict.createdDate
        cell.lblDetails.text = dict.description
        
        return cell
        
    }
    
    
}



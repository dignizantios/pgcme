//
//  AnnounementTblCell.swift
//  PGCME
//
//  Created by YASH on 26/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class AnnounementTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    
    //MARK: - View life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblTitle.textColor = UIColor.appThemeGrayColor
        lblTitle.font = themeFont(size: 16, fontname: .medium)
        
        [lblDate,lblDetails].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeGrayColor
            lbl?.font = themeFont(size: 12, fontname: .light)

        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

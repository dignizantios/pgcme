import Foundation 
import ObjectMapper 

class AnnouncementModel: Mappable { 

	var list = [AnnouncementList]()

    required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		list <- map["data"]
	}
} 

class AnnouncementList: Mappable {

	var announceId: String = ""
	var title: String = ""
	var description: String = ""
	var createdDate: String = ""

    required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		announceId <- map["announce_id"] 
		title <- map["title"] 
		description <- map["description"] 
		createdDate <- map["created_date"] 
	}
} 


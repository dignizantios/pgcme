
//
//  ChangePasswordViewModel.swift
//  PGCME
//
//  Created by YASH on 16/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation

class ChangePasswordViewModel{
    
    //MARK: - view life cycle
    
    init(){
        
    }
    
}

//MARK: - Service

extension ChangePasswordViewModel{
    
    func changePasswordAPICalling(param:[String:Any],completion:@escaping(String)->Void){
        
        let url = kBaseURL + kChangePassword
        
        CommonService().Service(url: url, param: param) { (response) in
            
            if let dictData = response.value{
                
                if dictData["flag"].stringValue == strSuccessResponse{
                    
                    makeToast(strMessage: dictData["msg"].stringValue)
                    completion("")
                    
                }else if dictData["flag"].stringValue == strAccessDenied{
                    completion(strAccessDenied)
                }else{
                    makeToast(strMessage: dictData["msg"].stringValue)
                }
            }
        }
        
    }
    
}

//
//  ChangePasswordVc.swift
//  PGCME
//
//  Created by YASH on 25/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class ChangePasswordVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblOldPassword: UILabel!
    @IBOutlet weak var txtoldPassword: CustomTextField!
    @IBOutlet weak var txtNewPassword: CustomTextField!
    @IBOutlet weak var lblNewPassword: UILabel!
    @IBOutlet weak var lblRetypePassword: UILabel!
    @IBOutlet weak var txtRetypePassword: CustomTextField!
    
    @IBOutlet weak var btnSubmit: CustomButton!
    
    //MARK: - Variable
    
    let viewModel = ChangePasswordViewModel()
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: "Change_Password_key".localized)
    }
    
    
    //MARK: - SetupUI
    
    func setupUI(){
        
        self.view.backgroundColor = UIColor.appThemeSkyBlueColor
        
        [lblOldPassword,lblNewPassword,lblRetypePassword].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeGrayColor
            lbl?.font = themeFont(size: 14, fontname: .medium)
        }
        
        lblOldPassword.text = "Old_Password_key".localized
        lblNewPassword.text = "New_Password_key".localized
        lblRetypePassword.text = "Re-type_Password_key".localized
        
        [txtoldPassword,txtNewPassword,txtRetypePassword].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.textColor = UIColor.appThemeDarkBlueColor
            txt?.placeholder = "Enter_here_key".localized
        }
        
        btnSubmit.setupThemeButtonUI()
        btnSubmit.setTitle("Submit_key".localized, for: .normal)
        
    }
    
    @IBAction func btnSubmitTapped(_ sender: CustomButton) {
        
        if (txtoldPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage:"Please_enter_old_password_key".localized)
        }else if (txtNewPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage:"Please_enter_new_password_key".localized)
        }else if (txtNewPassword.text?.count)! < passwordLegth {
            makeToast(strMessage: "New_password_must_be_at_least_eight_characters_long_key".localized)
        }
        /*else if (txtRetypePassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage:"Please_enter_re_type_password_key".localized)
        }
        else if (txtoldPassword.text?.count)! < passwordLegth || (txtNewPassword.text?.count)! < passwordLegth || (txtRetypePassword.text?.count)! < passwordLegth {
            makeToast(strMessage: "Password_must_be_at_least_eight_characters_long_key".localized)
        }*/
        else if(!(txtRetypePassword.text! == txtNewPassword.text!)) {
            makeToast(strMessage:  "New_password_and_re_type_password_must_match_key".localized)
        }else
        {
            
            let param = ["lang" : lang,
                         "user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "current_password" : txtoldPassword.text!,
                         "new_password" : txtNewPassword.text!
                        ]
            
            viewModel.changePasswordAPICalling(param: param) { [weak self] msg in
                if msg == strAccessDenied {
                    self?.logoutAPICalling()
                } else {
                   self?.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    
}

//MARK: - IBAction

extension ChangePasswordVc{
    
    @IBAction func btnHideShowOldPasswordTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        txtoldPassword.isSecureTextEntry = sender.isSelected ? false : true
    }
    
    @IBAction func btnHideShowNewPasswordTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        txtNewPassword.isSecureTextEntry = sender.isSelected ? false : true
    }
    
    @IBAction func btnHideShowRetyprPasswordTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        txtRetypePassword.isSecureTextEntry = sender.isSelected ? false : true
    }
    
}

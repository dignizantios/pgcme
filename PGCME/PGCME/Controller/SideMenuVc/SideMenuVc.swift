//
//  SideMenuVc.swift
//  PGCME
//
//  Created by YASH on 22/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class SideMenuVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet var vwHeader: UIView!
    @IBOutlet weak var lblPGCME: UILabel!
    
    @IBOutlet var vwFooter: UIView!
    @IBOutlet weak var btnLogout: UIButton!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Varaible
    
    var array : [JSON] = []
    
    
    //AMRK: -View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
    }

    func setupUI(){
        
        self.tableView.tableHeaderView = vwHeader
        self.tableView.register(UINib(nibName: "SideMenuTableCell", bundle: nil), forCellReuseIdentifier: "SideMenuTableCell")
        
        self.lblPGCME.textColor = UIColor.white
        self.lblPGCME.font = themeFont(size: 19, fontname: .medium)
        self.lblPGCME.text = "PG_CME_key".localized
        
        self.btnLogout.setTitle(("   " + "Logout_key".localized), for: .normal)
        self.btnLogout.titleLabel?.font = themeFont(size: 18, fontname: .medium)
        self.btnLogout.setTitleColor(UIColor.appThemeDarkBlueColor, for: .normal)
        
        self.tableView.backgroundColor = UIColor.appThemeDarkBlueColor
        
        setArray()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setArray), name: .didRefreshSideMenu, object: nil)
    }
    
    @objc func setArray(){
        
        array = []
        
        var dict = JSON()
        dict["name"].stringValue = "Home_key".localized
        self.array.append(dict)
        
        if dictSideMenu["register_menu"].stringValue == "1" {
            dict = JSON()
            dict["name"].stringValue = "Delegate_Registration_key".localized
            self.array.append(dict)
        }
        
        if dictSideMenu["login_menu"].stringValue == "1" {
            dict = JSON()
            dict["name"].stringValue = "Delegate_Login_key".localized
            self.array.append(dict)
        }
        
        if dictSideMenu["my_account_menu"].stringValue == "1" {
            dict = JSON()
            dict["name"].stringValue = "My_Account_key".localized
            self.array.append(dict)
        }

        if dictSideMenu["education_material_menu"].stringValue == "1" {
           dict = JSON()
            dict["name"].stringValue = "Education_Materials_key".localized
            self.array.append(dict)
        }
        
        if dictSideMenu["announcement_menu"].stringValue == "1" {
            dict = JSON()
            dict["name"].stringValue = "Announcements_key".localized
            self.array.append(dict)
        }
        
        if dictSideMenu["pre_question_menu"].stringValue == "1" {
            dict = JSON()
            dict["name"].stringValue = "Pre_questionnaries_key".localized
            self.array.append(dict)
        }
        
        if dictSideMenu["post_question_menu"].stringValue == "1" {
            dict = JSON()
            dict["name"].stringValue = "Post_questionnaries_key".localized
            self.array.append(dict)
        }
        
        if dictSideMenu["quiz_menu"].stringValue == "1" {
            dict = JSON()
            dict["name"].stringValue = "Quiz_key".localized
            self.array.append(dict)
        }
        
        if dictSideMenu["feedback_menu"].stringValue == "1" {
            dict = JSON()
            dict["name"].stringValue = "Feedback_Form_key".localized
            self.array.append(dict)
        }
        
        if dictSideMenu["logout_menu"].stringValue == "1" {
            self.tableView.tableFooterView = vwFooter
        } else {
           self.tableView.tableFooterView = UIView()
        }
 
        self.tableView.reloadData()
        
    }
    
}

//MARK: - IBAction

extension SideMenuVc{
    
    @IBAction func btnHideSideMenuTapped(_ sender: UIButton) {
        sideMenuController?.leftViewController?.hideLeftViewAnimated(true)
    }
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "", message: "Are_you_sure_you_want_to_logout?_key".localized, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Yes_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.logoutAPICalling()
            
        }
        let cancelAction = UIAlertAction(title: "No_key".localized, style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func redirectController(strName:String){
        
        if strName == "Home_key".localized{
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigateToSideMenuSelectedController(obj: obj)
            
        }else if strName == "Delegate_Registration_key".localized{
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVc") as! RegisterVc
            self.navigateToSideMenuSelectedController(obj: obj)
            
        }else if strName == "Delegate_Login_key".localized{
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
            self.navigateToSideMenuSelectedController(obj: obj)
            
        }else if strName == "My_Account_key".localized{
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyAccountVc") as! MyAccountVc
            self.navigateToSideMenuSelectedController(obj: obj)
            
        }else if strName == "Education_Materials_key".localized{
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "EducationMaterialVc") as! EducationMaterialVc
            self.navigateToSideMenuSelectedController(obj: obj)
            
        }else if strName == "Announcements_key".localized{
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AnnouncementVc") as! AnnouncementVc
            self.navigateToSideMenuSelectedController(obj: obj)
            
        }else if strName == "Pre_questionnaries_key".localized{
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "PreQuestionarriesVc") as! PreQuestionarriesVc
            obj.selectedController = .preQuestionnaries
            obj.selectedQuestionnriesStatus = .startQuestionnaries
            self.navigateToSideMenuSelectedController(obj: obj)
            
        }else if strName == "Post_questionnaries_key".localized{
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "PreQuestionarriesVc") as! PreQuestionarriesVc
            obj.selectedController = .postQuestionnaries
            obj.selectedQuestionnriesStatus = .alreadySubmitedQuestionnaries
            self.navigateToSideMenuSelectedController(obj: obj)
            
        }else if strName == "Quiz_key".localized{
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "PreQuestionarriesVc") as! PreQuestionarriesVc
            obj.selectedController = .quiz
            obj.selectedQuestionnriesStatus = .none
            self.navigateToSideMenuSelectedController(obj: obj)
            
        }else if strName == "Feedback_Form_key".localized{
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "PreQuestionarriesVc") as! PreQuestionarriesVc
            obj.selectedController = .feedback
            self.navigateToSideMenuSelectedController(obj: obj)
            
        }
        
    }
    
}


//MARK: - TableView Delegate

extension SideMenuVc: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableCell") as! SideMenuTableCell
        
        let dict = array[indexPath.row]
        cell.lblSideMenu.text = dict["name"].stringValue
        cell.lblCount.isHidden = true
        if dict["name"].stringValue == "Announcements_key".localized {
            cell.lblCount.isHidden = dictSideMenu["announcement_count"].intValue <= 0 ? true : false
            cell.lblCount.text = dictSideMenu["announcement_count"].stringValue
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = array[indexPath.row]
        self.redirectController(strName: dict["name"].stringValue)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
}

//MARK:- Service

extension SideMenuVc {
    
}

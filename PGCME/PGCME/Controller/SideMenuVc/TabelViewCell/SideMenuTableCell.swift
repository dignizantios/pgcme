//
//  SideMenuTableCell.swift
//  Liber
//
//  Created by YASH on 22/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class SideMenuTableCell: UITableViewCell {

    @IBOutlet weak var lblSideMenu: UILabel!
    @IBOutlet weak var lblCount: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblSideMenu.textColor = UIColor.white
        lblSideMenu.font = themeFont(size: 17, fontname: .medium)
        
        lblCount.textColor = UIColor.white
        lblCount.font = themeFont(size: 14, fontname: .medium)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  PreQuesdtionariesViewModel.swift
//  PGCME
//
//  Created by YASH on 17/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation
import SwiftyJSON

class PreQuesdtionariesViewModel{
    
    init() {
        
    }
}

extension PreQuesdtionariesViewModel{
    
    func checkQuestionariesAvailableorNotAPICalling(param:[String:Any],completion:@escaping(JSON,String)->Void){
        
        let url = kBaseURL + kQuestionariesFlag
        
        CommonService().Service(url: url, param: param) { (response) in
            
            if let dictData = response.value{
                
                if dictData["flag"].stringValue == strSuccessResponse{
                    
                    completion(JSON(dictData),dictData["flag"].stringValue)
                    
                }else if dictData["flag"].stringValue == strAccessDenied{
                    completion(JSON(),strAccessDenied)
                }else{
                    makeToast(strMessage: dictData["msg"].stringValue)
                }
            }
        }
        
    }
    
    func feedbackListAPICalling(param:[String:Any],completion:@escaping(JSON,String)->Void){
        
        let url = kBaseURL + kFeedbackList
        
        CommonService().Service(url: url, param: param) { (response) in
            
            if let dictData = response.value{
                
                if dictData["flag"].stringValue == strSuccessResponse{
                    completion(JSON(dictData),dictData["flag"].stringValue)
                }else if dictData["flag"].stringValue == strAccessDenied{
                    completion(JSON(),strAccessDenied)
                }else{
                    completion(JSON(dictData),dictData["flag"].stringValue)
//                    makeToast(strMessage: dictData["msg"].stringValue)
                }
            }
        }
    }
}

//
//  FeedBackTableCell.swift
//  PGCME
//
//  Created by Vishal on 27/02/20.
//  Copyright © 2020 Yash. All rights reserved.
//

import UIKit

class FeedBackListTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var vwMain: CustomView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkBlueColor
            lbl?.font = themeFont(size: 16, fontname: .medium)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

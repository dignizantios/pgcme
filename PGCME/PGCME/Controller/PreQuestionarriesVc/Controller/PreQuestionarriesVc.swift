//
//  PreQuestionarriesVc.swift
//  PGCME
//
//  Created by YASH on 26/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import SwiftyJSON

enum comeFromParentController:String{
    case preQuestionnaries = "0"
    case postQuestionnaries = "1"
    case quiz = "2"
    case feedback
}

enum questionarriesStatus{
    case startQuestionnaries
    case alreadySubmitedQuestionnaries
    case none
}


class PreQuestionarriesVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var vwButton: UIView!
    @IBOutlet weak var btnStartQuestion: CustomButton!
    @IBOutlet weak var vwfeedback: UIView!
    @IBOutlet weak var tblFeedback: UITableView!
    
    //MARK: - Variable
    
    var selectedController = comeFromParentController.preQuestionnaries
    var selectedQuestionnriesStatus = questionarriesStatus.startQuestionnaries
    
    let viewModel = PreQuesdtionariesViewModel()
    var strQuestionariedID = String()
    var remainingTimer : Timer?
    var remainngTimeInterval = Int()
    var dictFeedBackData = FeedbackModel()
    var arrayFeedBackData: [FeedbackData] = []
    var strMessage = ""
    var pageOffset = 0
    var refreshController = UIRefreshControl()
    
    
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        registerXib()
        
        self.refreshController = UIRefreshControl()
        self.refreshController.backgroundColor = UIColor.clear
        self.refreshController.tintColor = UIColor.appThemeDarkBlueColor
        self.refreshController.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        tblFeedback.addSubview(self.refreshController)
    }
    
    @objc func pullToRefresh() {
        self.pageOffset = 0
        self.arrayFeedBackData = []
        self.tblFeedback.reloadData()
        feedbackListAPI(offset: self.pageOffset)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        remainingTimer?.invalidate()
        remainingTimer = nil
    }
    
    func registerXib() {
        self.tblFeedback.register(UINib(nibName: "FeedBackListTableCell", bundle: nil), forCellReuseIdentifier: "FeedBackListTableCell")
    }
    
    func APICalling() {
        
        self.vwMain.isHidden = false
        
        var param = ["lang" : lang,
                     "timezone" : getCurrentTimeZone(),
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token")
                    ]
        
        if selectedController == .preQuestionnaries{
            param["is_flag"] = "0"
        }else if selectedController == .postQuestionnaries{
            param["is_flag"] = "1"
        }else if selectedController == .quiz{
            param["is_flag"] = "2"
        }else if selectedController == .feedback{
            param["is_flag"] = "3"
        }
        
        viewModel.checkQuestionariesAvailableorNotAPICalling(param: param){[weak self] (json,msg) in
            
            if msg ==  strAccessDenied {
                self?.logoutAPICalling()
            } else {
                print("json:\(json)")
                
                self?.lblMessage.text = json["msg"].stringValue
                self?.strQuestionariedID = json["questionnaires_id"].stringValue
                if json["is_questionaries"].stringValue == "0"{
                    self?.selectedQuestionnriesStatus = .none
                    self?.vwButton.isHidden = true
                }else if json["is_questionaries"].stringValue == "1"{
                    self?.selectedQuestionnriesStatus = .startQuestionnaries
                    self?.btnStartQuestion.setTitle("Start_Question_key".localized, for: .normal)
                    self?.vwButton.isHidden = false
                    self?.getRemainingIntervalFromDate(strTime: json["remaining_time"].stringValue)
                }else  if json["is_questionaries"].stringValue == "2"{
                    self?.selectedQuestionnriesStatus = .alreadySubmitedQuestionnaries
                    self?.btnStartQuestion.setTitle("View_Questionnaries_key".localized, for: .normal)
                    self?.vwButton.isHidden = true
                }
            }            
        }
    }
    
    func feedbackListAPI(offset: Int) {
        
        self.vwfeedback.isHidden = false
        
        let param = ["lang" : lang,
                     "timezone" : getCurrentTimeZone(),
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "offset":offset
            ] as [String : Any]
        
        viewModel.feedbackListAPICalling(param: param){[weak self] (json,msg) in
            self?.tblFeedback.tableFooterView = UIView()
            if msg ==  strAccessDenied {
                self?.logoutAPICalling()
            } else if msg == "0" {
                self?.strMessage = json["msg"].stringValue
                self?.arrayFeedBackData = []
                self?.tblFeedback.reloadData()
            } else {
                print("json:\(json)")
                self?.strMessage = json["msg"].stringValue
                self?.pageOffset = json["next_offset"].intValue
                
                let value = json["data"]
                
                if self?.pageOffset != -1 {
                    var arrayData : [FeedbackData] = []
                    for i in 0..<value.count {
                        let data = FeedbackData(JSON: JSON(value[i]).dictionaryObject!)
                        arrayData.append(data!)
                    }
                    self?.arrayFeedBackData += arrayData
                }
                else {
                    for i in 0..<value.count {
                        let data = FeedbackData(JSON: JSON(value[i]).dictionaryObject!)
                        self?.arrayFeedBackData.append(data!)
                    }
                }
                self?.refreshController.endRefreshing()
                self?.tblFeedback.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if selectedController == .preQuestionnaries{
            APICalling()
            setupNavigationbarwithSideMenuButton(titleText: "Pre_questionnaries_key".localized)
            
        }else if selectedController == .postQuestionnaries{
            APICalling()
            setupNavigationbarwithSideMenuButton(titleText: "Post_questionnaries_key".localized)
            
        }else if selectedController == .quiz{
            APICalling()
            setupNavigationbarwithSideMenuButton(titleText: "Quiz_key".localized)
        }else if selectedController == .feedback{
            feedbackListAPI(offset: self.pageOffset)
            setupNavigationbarwithSideMenuButton(titleText: "Feedback_Form_key".localized)
        }
        
//        APICalling()
        
    }
    
    func setupUI(){
        self.vwMain.isHidden = true
        self.vwfeedback.isHidden = true
        self.view.backgroundColor = UIColor.appThemeSkyBlueColor
        lblMessage.textColor = UIColor.appThemeGrayColor.withAlphaComponent(0.5)
        lblMessage.font = themeFont(size: 19, fontname: .bold)
        lblMessage.text = ""
        btnStartQuestion.setupThemeButtonUI()
        self.vwButton.isHidden = true
    }
    
    func getRemainingIntervalFromDate(strTime:String){
        
        let date = StringToDate(Formatter: enumDateFormatter.dateFormatter2.rawValue, strDate: strTime)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        
        remainngTimeInterval = (hour*60*60) + (minute*60) + second
        //        totalTimeInterval = remainngTimeInterval
        remainingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateRemainingTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateRemainingTimer() {
        if remainngTimeInterval <= 0{
            if remainingTimer != nil {
                remainingTimer?.invalidate()
                remainingTimer = nil
            }
        } else {
            remainngTimeInterval -= 1
            print("Remaining Timer = \(timeString(time: TimeInterval(remainngTimeInterval)))")
        }
    }

    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
}

//MARK: - IBAction method

extension PreQuestionarriesVc{
    
    @IBAction func btnQuestionTapped(_ sender: UIButton) {
        
        if remainngTimeInterval <= 0 {
            APICalling()
            return
        }
        if selectedController == .feedback{
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackFormVC") as! FeedbackFormVC
            obj.setupData(id: strQuestionariedID)
            self.navigationController?.pushViewController(obj, animated: true)
            
        } else {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "QuestionnariesDetailsVc") as! QuestionnariesDetailsVc
            obj.selectedController = self.selectedController
            obj.selectedQuestionnariesStatus = self.selectedQuestionnriesStatus
            obj.setupData(id: strQuestionariedID)
            self.navigationController?.pushViewController(obj, animated: true)
        }

    }
    
}

extension PreQuestionarriesVc : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedController == .feedback {
            if arrayFeedBackData.count == 0 {
                let lbl = UILabel()
                lbl.text = self.strMessage
                lbl.font = themeFont(size: 19, fontname: .bold)
                lbl.textAlignment = NSTextAlignment.center
                lbl.textColor = UIColor.appThemeGrayColor.withAlphaComponent(0.5)
                lbl.center = tableView.center
                lbl.numberOfLines = 0
                tableView.backgroundView = lbl
                return 0
            }
        }
        tableView.backgroundView = nil
        return self.arrayFeedBackData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedBackListTableCell", for: indexPath) as! FeedBackListTableCell
        
        let dict = self.arrayFeedBackData[indexPath.row]
        cell.lblTitle.text = dict.title
        
        if dict.isQuestionaries == "2" {
            cell.vwMain.alpha = 0.6
        }
        else {
            cell.vwMain.alpha = 1.0
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = self.arrayFeedBackData[indexPath.row]
        
        if dict.isQuestionaries == "2" {
            makeToast(strMessage: "This form is already submitted.")
        }
        else {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackFormVC") as! FeedbackFormVC
            obj.setupData(id: dict.questionnairesId)
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if self.arrayFeedBackData.count-1 == indexPath.row && self.pageOffset != -1 {
           
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            self.feedbackListAPI(offset: self.pageOffset)
        }
    }
}

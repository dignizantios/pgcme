import Foundation 
import ObjectMapper 

class FeedbackModel: Mappable { 

	var data: [FeedbackData] = []
	var msg: String = ""
	var flag: NSNumber = 0
	var nextOffset: NSNumber = 0
    

	required convenience init?(map: Map){
        self.init()
	} 

	func mapping(map: Map) {
		data <- map["data"] 
		msg <- map["msg"] 
		flag <- map["flag"]
		nextOffset <- map["next_offset"]
	}
} 

class FeedbackData: Mappable {
    
    var isQuestionaries: String = ""
	var questionnairesName: String = ""
	var finishDatetime: String = ""
	var questionnairesId: String = ""
	var startDatetime: String = ""
	var title: String = ""

	required convenience init?(map: Map){
        self.init()
	} 

	func mapping(map: Map) {
        
        isQuestionaries <- map["is_questionaries"]
		questionnairesName <- map["questionnaires_name"] 
		finishDatetime <- map["finish_datetime"] 
		questionnairesId <- map["questionnaires_id"] 
		startDatetime <- map["start_datetime"] 
		title <- map["title"] 
	}
} 


import Foundation 
import ObjectMapper 

class QuestionariesDetailModel: Mappable { 

	var flag: Int = 0
	var msg: String = ""
	var isQuestionaries: String = ""
	var questionnairesId: String = ""
	var startDatetime: String = ""
	var finishDatetime: String = ""
	var graceTime: String = ""
	var data = [QuestionariesModelList]()
    var remainingTime:String = ""
    var remainingFlag:String = ""
    var title : String = ""
    

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		isQuestionaries <- map["is_questionaries"] 
		questionnairesId <- map["questionnaires_id"] 
		startDatetime <- map["start_datetime"] 
		finishDatetime <- map["finish_datetime"] 
		graceTime <- map["grace_time"]
        remainingTime <- map["remaining_time"]
        remainingFlag <- map["remaining_flag"]
		data <- map["data"]
        title <- map["title"]
	}
} 

class QuestionariesModelList: Mappable {

	var questionsId: String = ""
	var questionName: String = ""
	var options = [Options]()   

	required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		questionsId <- map["questions_id"] 
		questionName <- map["question_name"] 
		options <- map["options"] 
	}
} 
 


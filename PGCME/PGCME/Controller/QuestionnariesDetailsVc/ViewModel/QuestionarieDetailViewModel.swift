//
//  QuestionarieDetailViewModel.swift
//  PGCME
//
//  Created by Jaydeep on 18/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation

class QuestionarieDetailViewModel {
    var strQuestionariedId = String()
    var arrQuestionariesDetail = [QuestionariesModelList]()
    var isFlag = String()
    var dictQuestionariedDetail = QuestionariesDetailModel()
}

extension QuestionarieDetailViewModel{
    
    func getQuestinariesDetail(id:String,time:String,completion:@escaping(String,String) -> Void){
        
        let url = kBaseURL + kGetQuestionariesList
        let param = ["lang" : lang,
                     "timezone" : getCurrentTimeZone(),
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "questionnaires_id" : "\(id)",
                     "is_flag":isFlag,
                     "time_check":time
                    ]
        
        CommonService().Service(url: url, param: param) { (respones) in
            
            if let json = respones.value
            {
                print("JSON : \(json)")
                
                if json["flag"].stringValue == strSuccessResponse
                {
                    if let dicData =  json.dictionaryObject , let questionariesDetail = QuestionariesDetailModel(JSON:dicData) {
                        self.arrQuestionariesDetail = questionariesDetail.data
                        self.dictQuestionariedDetail = questionariesDetail
                    }
                    completion("","")
                }
                else if json["flag"].stringValue == strAccessDenied
                {
                    completion(strAccessDenied, "")
                } else if json["flag"].stringValue == "3"{
                    completion(json["flag"].stringValue, json["msg"].stringValue)
                    makeToast(strMessage: json["msg"].stringValue)
                } else if json["flag"].stringValue == "-2"{
                    completion(json["flag"].stringValue, json["msg"].stringValue)
//                    makeToast(strMessage: json["msg"].stringValue)
                } else {
                    completion(json["flag"].stringValue, json["msg"].stringValue)
                    makeToast(strMessage: json["msg"].stringValue)
                }
            }
            else
            {            completion("-2","Server_not_responding_Please_try_again_later_key".localized)
//                makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
            }
        }
    }
    
    func submitAnswer(dict:[String:Any],completion:@escaping(String,String) -> Void){
        
        let url = kBaseURL + kPostQuestionAnswer
        
        CommonService().Service(url: url, param: dict) { (respones) in
            
            if let json = respones.value
            {
                print("JSON : \(json)")
                
                if json["flag"].stringValue == strSuccessResponse {
                    completion(json["msg"].stringValue,json["flag"].stringValue)
                    makeToast(strMessage: json["msg"].stringValue)
                } else if json["flag"].stringValue == strAccessDenied {
                    completion(json["msg"].stringValue,json["flag"].stringValue)
                } else if json["flag"].stringValue == "-2" {
                    completion(json["msg"].stringValue,json["flag"].stringValue)
                } else {
                    completion(json["msg"].stringValue,json["flag"].stringValue)
                    makeToast(strMessage: json["msg"].stringValue)
                }
            }
            else
            {
                completion("-2","Server_not_responding_Please_try_again_later_key".localized)
//                makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
            }
        }
    }
}

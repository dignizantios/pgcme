//
//  QuestionnariesDetailsVc.swift
//  PGCME
//
//  Created by YASH on 26/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import SwiftyJSON

class QuestionnariesDetailsVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var vwSubmit: UIView!
    @IBOutlet weak var btnSubmit: CustomButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: - Variable
    
    var selectedController = comeFromParentController.preQuestionnaries
    var selectedQuestionnariesStatus = questionarriesStatus.startQuestionnaries
    var questinariedViewModel = QuestionarieDetailViewModel()
    var strMessage = String()
    var lblTimeCounter = UILabel()
    var graceTimer : Timer?
    var timeInterval = Int()
    var totalTimeInterval = Int()
    var remainingTimer : Timer?
    var remainngTimeInterval = Int()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        APICalling()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if graceTimer != nil {
            graceTimer?.invalidate()
            graceTimer = nil
        }
        
        if remainingTimer != nil {
            remainingTimer?.invalidate()
            remainingTimer = nil
        }
    }

    func setupUI(){
        
        self.view.backgroundColor = UIColor.appThemeSkyBlueColor
        
        self.tableView.register(UINib(nibName: "QuestionaryHeaderCell", bundle: nil), forCellReuseIdentifier: "QuestionaryHeaderCell")
        self.tableView.tableFooterView = UIView()
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkBlueColor
            lbl?.font = themeFont(size: 16, fontname: .medium)
        }
        lblTitle.text = ""
        btnSubmit.setTitle("Submit_key".localized, for: .normal)
        btnSubmit.setupThemeButtonUI()
  
        if selectedQuestionnariesStatus == .startQuestionnaries{

        }else if selectedQuestionnariesStatus == .alreadySubmitedQuestionnaries{

            vwSubmit.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if selectedController == .preQuestionnaries{
            setupNavigationbarwithBackButton(titleText: "Pre_questionnaries_key".localized)
        }else if selectedController == .postQuestionnaries{
            setupNavigationbarwithBackButton(titleText: "Post_questionnaries_key".localized)
        }else if selectedController == .quiz{
            setupNavigationbarwithBackButton(titleText: "Quiz_key".localized)
        }
    }
    
    func setUpData() {
        
        self.lblTitle.text = self.questinariedViewModel.dictQuestionariedDetail.title
    }
    
    override func viewDidLayoutSubviews() {
        self.tableView.reloadData()
    }
    
    func APICalling()  {
        
        var strFinalTime = String()
        if selectedController == .preQuestionnaries {
           if let strTime = Defaults.value(forKey: "pre_questionnaries_time") as? String{
                strFinalTime = strTime
            } else {
                strFinalTime = "00:00:00"
            }
        } else if selectedController == .postQuestionnaries {
            if let strTime = Defaults.value(forKey: "post_questionnaries_time") as? String{
                strFinalTime = strTime
            } else {
                strFinalTime = "00:00:00"
            }
        } else if selectedController == .quiz {
           if let strTime = Defaults.value(forKey: "quize_time") as? String{
                strFinalTime = strTime
            } else {
                strFinalTime = "00:00:00"
            }
        }        
        
        questinariedViewModel.getQuestinariesDetail(id: questinariedViewModel.strQuestionariedId, time: strFinalTime) { (error,msg) in
            if error == strAccessDenied {
                self.logoutAPICalling()
            } else if error == "3" {
                self.showPopupMessage(msg: msg)
            } else if error == "-2" {
                self.showPopupMessage(msg: msg)
            } else {
                for i in 0..<self.questinariedViewModel.arrQuestionariesDetail.count {
                    let dict = self.questinariedViewModel.arrQuestionariesDetail[i]
                    for j in 0..<dict.options.count{
                        let dictOption = dict.options[j]
                        dictOption.isSelected = false
                        dict.options[j] = dictOption
                    }
                    self.questinariedViewModel.arrQuestionariesDetail[i] = dict
                }
                if self.questinariedViewModel.dictQuestionariedDetail.remainingTime != "" || self.questinariedViewModel.dictQuestionariedDetail.remainingTime != "00:00:00"{
                    self.lblTimeCounter = UILabel.init(frame: CGRect(x: 0, y: 0, width: 70, height: 20))
                    self.lblTimeCounter.textColor = .white
                    self.lblTimeCounter.font = themeFont(size: 15, fontname: .medium)
                    self.lblTimeCounter.textAlignment = .right
                    self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.lblTimeCounter)
                    self.getRemainingIntervalFromDate()
                } else {
                    self.getIntervalFromDate()
                }
                self.strMessage = error
                self.tableView.reloadData()
                self.setUpData()
                
                if self.questinariedViewModel.dictQuestionariedDetail.remainingFlag == "1" {
//                    self.getRemainingIntervalFromDate()
                }
            }
        }
    }
    
    func getIntervalFromDate(){
        let strTime = self.questinariedViewModel.dictQuestionariedDetail.graceTime
       
        let date = StringToDate(Formatter: enumDateFormatter.dateFormatter2.rawValue, strDate: strTime)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        
        timeInterval = (hour*60*60) + (minute*60) + second
        totalTimeInterval = timeInterval
        graceTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    func getRemainingIntervalFromDate(){
        let strTime = self.questinariedViewModel.dictQuestionariedDetail.remainingTime
        
        let date = StringToDate(Formatter: enumDateFormatter.dateFormatter2.rawValue, strDate: strTime)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        
        remainngTimeInterval = (hour*60*60) + (minute*60) + second
        //        totalTimeInterval = remainngTimeInterval
        remainingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateRemainingTimer), userInfo: nil, repeats: true)
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    @objc func updateTimer() {
        if timeInterval <= 0{
            if graceTimer != nil {
                graceTimer?.invalidate()
                graceTimer = nil
//                self.submitAnswer()
            }
        } else {
            timeInterval -= 1
            print("Grace Timer = \(timeString(time: TimeInterval(timeInterval)))")
//            self.lblTimeCounter.text = "\(timeString(time: TimeInterval(timeInterval)))"
            if selectedController == .preQuestionnaries {
                Defaults.setValue(timeString(time: TimeInterval(totalTimeInterval - timeInterval)), forKey: "pre_questionnaries_time")
                Defaults.synchronize()
            } else if selectedController == .postQuestionnaries {
                Defaults.setValue(timeString(time: TimeInterval(totalTimeInterval - timeInterval)), forKey: "post_questionnaries_time")
                Defaults.synchronize()
            } else if selectedController == .quiz {
                Defaults.setValue(timeString(time: TimeInterval(totalTimeInterval - timeInterval)), forKey: "quize_time")
                Defaults.synchronize()
            }
        }
    }
    
    @objc func updateRemainingTimer() {
        if remainngTimeInterval <= 0{
            if remainingTimer != nil {
                remainingTimer?.invalidate()
                remainingTimer = nil
                getIntervalFromDate()
            }
            if selectedController == .preQuestionnaries {
                Defaults.setValue("00:00:00", forKey: "pre_questionnaries_time")
                Defaults.synchronize()
            } else if selectedController == .postQuestionnaries {
                Defaults.setValue("00:00:00", forKey: "post_questionnaries_time")
                Defaults.synchronize()
            } else if selectedController == .quiz {
                Defaults.setValue("00:00:00", forKey: "quize_time")
                Defaults.synchronize()
            }
            self.lblTimeCounter.text = ""
//            showPopupMessage(msg: "Sorry_the_allotted_time_exceeds_key".localized)
        } else {
            remainngTimeInterval -= 1
            self.lblTimeCounter.text = "\(timeString(time: TimeInterval(remainngTimeInterval)))"
            print("Remaining Timer = \(timeString(time: TimeInterval(remainngTimeInterval)))")
        }
    }
    
    func showPopupMessage(msg:String){
        let alertController = UIAlertController(title: "PG_CME_key".localized, message: msg, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in

           let obj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
           self.navigateToSideMenuSelectedController(obj: obj)
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    /*override func backButtonTapped() {
        
        let alertController = UIAlertController(title: "", message: "Are_you_sure_you_want_to_quite_questionary?_key".localized, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Yes_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in

            self.submitAnswer()
            
        }
            
        let cancelAction = UIAlertAction(title: "No_key".localized, style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }*/
    
    func setupData(id:String){
        questinariedViewModel.strQuestionariedId = id
        if selectedController == .preQuestionnaries{
            questinariedViewModel.isFlag = "0"
        }else if selectedController == .postQuestionnaries{
            questinariedViewModel.isFlag = "1"
        }else if selectedController == .quiz{
            questinariedViewModel.isFlag = "2"
        }
    }
 
}

//MARK: - IBAction method

extension QuestionnariesDetailsVc{
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        if remainngTimeInterval <= 0 && timeInterval <= 0{
            showPopupMessage(msg: "Sorry_the_allotted_time_exceeds_key".localized)
            return
        }
        
        var isOneAnswerSelected = false
        for i in 0..<questinariedViewModel.arrQuestionariesDetail.count {
            let arrOption = questinariedViewModel.arrQuestionariesDetail[i].options
            for j in 0..<arrOption.count {
                let dictInner = arrOption[j]
                if dictInner.isSelected{
                    isOneAnswerSelected = true
                    break
                }
            }
        }
        
        if isOneAnswerSelected == false{
            makeToast(strMessage: "Atleast_one_question_must_be_answered_to_submit_the_form_key".localized)
        } else {
            submitAnswer()
        }
    }
    
    func submitAnswer(){
        var dict:[String:Any] = [:]
        dict["user_id"] = getUserDetail("user_id")
        dict["access_token"] = getUserDetail("access_token")
        dict["timezone"] = getCurrentTimeZone()
        dict["lang"] = lang
        dict["questionnaires_id"] = questinariedViewModel.strQuestionariedId
//        dict["time_duration"] = "\(self.lblTimeCounter.text ?? "00:00:00")"
//        dict["fillup_time"] = "\(timeString(time: TimeInterval(totalTimeInterval - timeInterval)))"
        dict["answer_json"] = JSON(createAnswerJson()).rawString()
        print("dict\(dict)")
        
        questinariedViewModel.submitAnswer(dict: dict) { (msg,flg) in
            if flg == strAccessDenied {
                self.logoutAPICalling()
            } else if flg == "-2" {
                self.showPopupMessage(msg: msg)
            } else {
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigateToSideMenuSelectedController(obj: obj)
            }
        }
    }
    
    func createAnswerJson() ->  [JSON] {
        var arrAnswerJson:[JSON] = []
        for i in 0..<questinariedViewModel.arrQuestionariesDetail.count {
            let arrOption = questinariedViewModel.arrQuestionariesDetail[i].options
            var dict = JSON()
            dict["questions_id"] = JSON(questinariedViewModel.arrQuestionariesDetail[i].questionsId)
            dict["answer"] = ""
            for j in 0..<arrOption.count {
                let dictInner = arrOption[j]
                if dictInner.isSelected{
                    dict["answer"] = JSON(dictInner.selectedIndex)
                }
            }
            arrAnswerJson.append(dict)
        }
        print("arrAnswerJson \(arrAnswerJson)")
        return arrAnswerJson
    }

}

//MARK: - TableView Delegate

extension QuestionnariesDetailsVc: UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView{
            if questinariedViewModel.arrQuestionariesDetail.count == 0{
                let lbl = UILabel()
                lbl.text = self.strMessage
                lbl.font = themeFont(size: 16, fontname: .medium)
                lbl.textAlignment = NSTextAlignment.center
                lbl.textColor = UIColor.appThemeGrayColor
                lbl.center = tableView.center
                tableView.backgroundView = lbl
                return 0
            }
            return questinariedViewModel.arrQuestionariesDetail.count
        }
        return questinariedViewModel.arrQuestionariesDetail[tableView.tag].options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tableView{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionaryHeaderCell") as! QuestionaryHeaderCell
            
            cell.lblIndex.text = "\(indexPath.row+1)."
            let dict = questinariedViewModel.arrQuestionariesDetail[indexPath.row]
            cell.txtViewQuestion.attributedText = dict.questionName.htmlToAttributedString
            
            cell.tblSubData.register(UINib(nibName: "QuestionryTblCell", bundle: nil), forCellReuseIdentifier: "QuestionryTblCell")
            cell.tblSubData.tag = indexPath.row
            cell.tblSubData.delegate = self
            cell.tblSubData.dataSource = self
            cell.tblSubData.reloadData()
            cell.tblSubData.layoutIfNeeded()
            cell.tblSubData.layoutSubviews()
            
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionryTblCell") as! QuestionryTblCell
        
        cell.vwRadio.isHidden = false
        
        let dict = questinariedViewModel.arrQuestionariesDetail[tableView.tag].options[indexPath.row]
        cell.txtViewSubName.attributedText = dict.optionsName.htmlToAttributedString
        if dict.isSelected {
            cell.btnRadio.isSelected = true
        } else {
            cell.btnRadio.isSelected = false
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.tableView{
            print("Dont do anything")
        }
        else{
            
            var arrOption = questinariedViewModel.arrQuestionariesDetail[tableView.tag].options
            for i in 0..<arrOption.count {
                let dict = arrOption[i]
                dict.isSelected = false
                arrOption[i] = dict
            }
            
            questinariedViewModel.arrQuestionariesDetail[tableView.tag].options = arrOption
            
            arrOption = questinariedViewModel.arrQuestionariesDetail[tableView.tag].options
            let dict = arrOption[indexPath.row]
            dict.isSelected = true
            dict.selectedIndex = indexPath.row+1
            arrOption[indexPath.row] = dict
            
            UIView.performWithoutAnimation {
                self.tableView.reloadData()
            }
        }
    }
}

//
//  EditProfileVc.swift
//  PGCME
//
//  Created by YASH on 25/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import TOCropViewController
import MobileCoreServices
import DropDown

class EditProfileVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblRegisterNumber: UILabel!
    @IBOutlet weak var txtRegister: CustomTextField!
    @IBOutlet weak var lblDR: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var lblInitial: UILabel!
    @IBOutlet weak var txtInitial: CustomTextField!
    @IBOutlet weak var btnMale: CustomButton!
    @IBOutlet weak var btnFemale: CustomButton!
    @IBOutlet weak var lblDateOfBirth: UILabel!
    @IBOutlet weak var txtDOB: CustomTextField!
    @IBOutlet weak var lblInstitution: UILabel!
    @IBOutlet weak var txtInstitution: CustomTextField!
    @IBOutlet weak var vwNameOfInstitution: UIView!
    @IBOutlet weak var lblNameOfInstitution: UILabel!
    @IBOutlet weak var txtNameOfInstitution: CustomTextField!
    @IBOutlet weak var lblCourse: UILabel!
    @IBOutlet weak var txtCourse: CustomTextField!
    @IBOutlet weak var lblYearOfStudy: UILabel!
    @IBOutlet weak var txtYearOfStudy: CustomTextField!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var txtAddress: CustomTextField!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var txtCity: CustomTextField!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var txtState: CustomTextField!
    @IBOutlet weak var lblPinCode: UILabel!
    @IBOutlet weak var txtPinCode: CustomTextField!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var txtUserName: CustomTextField!
    @IBOutlet weak var lblEmailID: UILabel!
    @IBOutlet weak var txtEmailID: CustomTextField!
    @IBOutlet weak var lblTelephoneNo: UILabel!
    @IBOutlet weak var txtTelePhoneNo: CustomTextField!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var txtMobileNo: CustomTextField!
    @IBOutlet weak var imgBoafide: UIImageView!
    
    @IBOutlet weak var lblBonafied: UILabel!
    @IBOutlet weak var btnSubmit: CustomButton!

    ///--New
    @IBOutlet weak var vwCourseDetails: UIView!
    @IBOutlet weak var vwSpecifyCource: UIView!
    @IBOutlet weak var lblSpecifyCource: UILabel!
    @IBOutlet weak var txtSpecifyCource: CustomTextField!
    
    @IBOutlet weak var vwSpecifyYear: UIView!
    @IBOutlet weak var lblSpecifyYear: UILabel!
    @IBOutlet weak var txtSpecifyYear: CustomTextField!
    
    @IBOutlet weak var vwCmeCrediteCerificate: UIView!
    @IBOutlet weak var lblCmeCrediteCerificate: UILabel!
    @IBOutlet weak var btnRequired: UIButton!
    @IBOutlet weak var btnNotRequired: UIButton!
    @IBOutlet weak var vwUploadBonafide: UIView!
    
    //MARK: - Variable
    
    private var customImagePicker = CustomImagePicker()
    var editProfileViewModel = EditProfileViewModel()
    var picker = UIImagePickerController()
    var courseDD = DropDown()
    var yearStudyDD = DropDown()
    let regiViewModel = RegisterViewModel()
    var handlerUpdateProfile:() -> Void = {}
    var pdfData = Data()
    var isPdf = Bool()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        setupData(theModel: editProfileViewModel.dictMyAccountData)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: "Edit_Profile_key".localized)
    }
    
    
    
    override func viewDidLayoutSubviews() {
        self.configureDropdown(dropdown: courseDD, sender:txtCourse)
        self.configureDropdown(dropdown: yearStudyDD, sender:txtYearOfStudy,isWidth:false)
    }

    //MARK: - SetupUI
    
    func setupUI(){
        
        self.view.backgroundColor = UIColor.appThemeSkyBlueColor
        
        imgBoafide.layer.cornerRadius = 8.0
        imgBoafide.layer.masksToBounds = true
        [lblRegisterNumber,lblName,lblInitial,lblDateOfBirth,lblInstitution,lblNameOfInstitution,lblCourse,lblYearOfStudy,lblAddress,lblCity,lblState,lblPinCode,lblUserName,lblEmailID,lblTelephoneNo,lblMobileNo,lblBonafied].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeGrayColor
            lbl?.font = themeFont(size: 14, fontname: .medium)
        }
        [txtRegister,txtName,txtInitial,txtDOB,txtInstitution,txtNameOfInstitution,txtCourse,txtYearOfStudy,txtAddress,txtCity,txtState,txtPinCode,txtUserName,txtEmailID,txtTelePhoneNo,txtMobileNo].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.textColor = UIColor.appThemeDarkBlueColor
            txt?.placeholder = "Enter_here_key".localized
            txt?.delegate = self
        }
        
        [txtTelePhoneNo,txtPinCode,txtMobileNo].forEach { (txtField) in
            addDoneButtonOnKeyboard(textfield: txtField!)
            txtField?.keyboardType = .phonePad
        }
        
        txtCourse.placeholder = "Select_here_key".localized
        txtYearOfStudy.placeholder = "Select_here_key".localized
        
        lblDR.textColor = UIColor.appThemeGrayColor
        lblDR.font = themeFont(size: 16, fontname: .medium)
        
        lblRegisterNumber.text = "Registration_Number_key".localized
        lblDR.text = "Dr_key".localized
        lblName.text = "Name_key".localized
        lblInitial.text = "Initial_key".localized
        lblDateOfBirth.text = "Date_of_Birth_key".localized
        lblInstitution.text = "Institution_key".localized
        lblNameOfInstitution.text = "Name_of_Institution_key".localized
        lblCourse.text = "Course_key".localized
        lblYearOfStudy.text = "Year_of_Study_key".localized
        lblAddress.text = "Address_key".localized
        lblCity.text = "City_key".localized
        lblState.text = "State_key".localized
        lblPinCode.text = "Pincode_key".localized
        lblUserName.text = "Username_key".localized
        lblEmailID.text = "Email_ID_key".localized
        lblTelephoneNo.text = "Telephone_No_key".localized
        lblMobileNo.text = "Mobile_No_key".localized
        lblBonafied.text = "Upload_Bonafide_Certificate_key".localized
        btnSubmit.setTitle("Submit_key".localized, for: .normal)
        btnSubmit.setupThemeButtonUI()
        
        [btnMale,btnFemale].forEach { (btn) in
            btn?.setTitleColor(UIColor.appThemeGrayColor, for: .normal)
            btn?.borderColor = UIColor.appThemeGrayColor
        }
        
        [txtInstitution, txtNameOfInstitution].forEach { (txt) in
            txt?.isUserInteractionEnabled = false
        }
        
        ///--vp
        
        [lblSpecifyCource, lblSpecifyYear, lblCmeCrediteCerificate].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeGrayColor
            lbl?.font = themeFont(size: 14, fontname: .medium)
        }
        
        [txtSpecifyYear, txtSpecifyCource].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.textColor = UIColor.appThemeDarkBlueColor
            txt?.placeholder = "Enter_here_key".localized
            txt?.delegate = self
        }
        
        [btnRequired, btnNotRequired].forEach { (btn) in
            btn?.setTitleColor(UIColor.appThemeDarkBlueColor, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .light)
        }
        
        self.vwCourseDetails.isHidden = true
        self.vwCmeCrediteCerificate.isHidden = true
        [vwSpecifyYear, vwSpecifyCource].forEach { (vw) in
            vw?.isHidden = true
        }
        btnNotRequired.isSelected = false
        ///--
        
    }

    func setupModel(theModel:myAccountDetail){
        editProfileViewModel.dictMyAccountData = theModel
    }
    
    func setupData(theModel:myAccountDetail) {
        self.txtRegister.text = theModel.registerNo
        self.txtName.text = theModel.name.replacingOccurrences(of: "Dr", with: "")
        self.btnMale.isSelected = theModel.sex == "Male" ? true : false
        self.btnFemale.isSelected = theModel.sex == "Female" ? true : false
        self.txtDOB.text = theModel.dob
        self.txtInstitution.text = theModel.institution
        self.txtNameOfInstitution.text = theModel.nameOfInstitute
        self.txtCourse.text = theModel.course
        self.txtYearOfStudy.text = theModel.yearStudy
        self.txtAddress.text = theModel.address
        self.txtEmailID.text = theModel.emailId
        self.txtCity.text = theModel.city
        self.txtState.text = theModel.state
        
        self.txtPinCode.text = theModel.pincode
        self.txtUserName.text = theModel.userName
        self.txtEmailID.text = theModel.emailId
        self.txtTelePhoneNo.text = theModel.telephone
        self.txtMobileNo.text = theModel.mobileNo
        if theModel.bonafide.contains(".pdf") {
            self.imgBoafide.image = UIImage(named: "ic_pdf")
        } else {
            self.imgBoafide.sd_setImage(with: URL(string: theModel.bonafide), placeholderImage: UIImage(named: "ic_bonafide _certificate"), options: .lowPriority, completed: nil)
        }
       // self.imgBoafide.sd_setImage(with: URL(string: theModel.bonafide), placeholderImage: UIImage(named: "ic_bonafide _certificate"), options: .lowPriority, completed: nil)
        
        if btnMale.isSelected{
            btnMale.borderColor = UIColor.appThemeDarkBlueColor
            btnMale.setTitleColor(UIColor.appThemeDarkBlueColor, for: .normal)
        }
        else{
            btnFemale.borderColor = UIColor.appThemeDarkBlueColor
            btnFemale.setTitleColor(UIColor.appThemeDarkBlueColor, for: .normal)
        }
        
        [txtUserName,txtEmailID,txtMobileNo,txtRegister].forEach { (txtField) in
            txtField?.isUserInteractionEnabled = false
        }
        
        if theModel.nameChangeValid == "0"{
            txtName.isUserInteractionEnabled = false
        }
        
        ///--vp
        [txtCourse, txtYearOfStudy].forEach { (txt) in
            txt?.isUserInteractionEnabled = false
        }
        
        self.txtSpecifyCource.text = theModel.courseOther
        self.txtSpecifyYear.text = theModel.studyOther
        if theModel.institution == "Other" {
            self.vwCmeCrediteCerificate.isHidden = false
            if theModel.credit == "Not Required" {
                self.btnRequired.isSelected = false
                self.btnNotRequired.isSelected = true
                self.vwUploadBonafide.isHidden = true
            }
            else {
                self.btnRequired.isSelected = true
                self.btnNotRequired.isSelected = false
                self.vwUploadBonafide.isHidden = false
            }
        }
        if theModel.yearStudy == "Other" || theModel.course == "Other" {
            self.vwCourseDetails.isHidden = false
            if self.txtCourse.text == "Other" {
                self.vwSpecifyCource.isHidden = false
            }
            else {
               self.txtSpecifyCource.text = ""
            }
            if self.txtYearOfStudy.text == "Other" {
                self.vwSpecifyYear.isHidden = false
            }
            else {
               self.txtSpecifyYear.text = ""
            }
        }
        else {
            self.txtSpecifyYear.text = ""
            self.txtSpecifyCource.text = ""
        }
        ///--
        
    }
    
    func setUpNewDataField() {
        
        self.vwCourseDetails.isHidden = true
        [vwSpecifyCource, vwSpecifyYear].forEach { (vw) in
            vw?.isHidden = true
        }
        if self.txtCourse.text == "Other" || self.txtYearOfStudy.text == "Other" {
            self.vwCourseDetails.isHidden = false
            
            if self.txtCourse.text == "Other" {
                self.vwSpecifyCource.isHidden = false
            }
            if self.txtYearOfStudy.text == "Other" {
                self.vwSpecifyYear.isHidden = false
            }
        }
    }
    
}


//MARK: - IBAction method

extension EditProfileVc{
    
    func btnSelectUnSelect(selectedBtn:CustomButton,unSelectedBtn:CustomButton){
        
        selectedBtn.isSelected = !selectedBtn.isSelected
        selectedBtn.borderColor = UIColor.appThemeDarkBlueColor
        selectedBtn.setTitleColor(selectedBtn.isSelected == true ? UIColor.appThemeDarkBlueColor : UIColor.appThemeGrayColor, for: .normal)
        
        unSelectedBtn.isSelected = false
        unSelectedBtn.borderColor = UIColor.appThemeGrayColor
        unSelectedBtn.setTitleColor(UIColor.appThemeGrayColor, for: .normal)
        
    }
    
    @IBAction func btnMaleTapped(_ sender: UIButton) {
        btnSelectUnSelect(selectedBtn: btnMale, unSelectedBtn: btnFemale)
    }
    
    @IBAction func btnFemaleTapped(_ sender: UIButton) {
        btnSelectUnSelect(selectedBtn: btnFemale, unSelectedBtn: btnMale)
    }
    
    @IBAction func btnUploadBonafideTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        openActionSheetOption()
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        if (txtName.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_name_key".localized)
        } else if self.btnMale.isSelected == false && self.btnFemale.isSelected == false{
            makeToast(strMessage: "Please_select_gender_key".localized)
        } else if txtDOB.text!.isEmpty {
            makeToast(strMessage: "Please_select_date_of_birth_key".localized)
        } else if (txtInstitution.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_institution_name_key".localized)
        } else if (txtNameOfInstitution.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_institution_name_key".localized)
        } else if txtCourse.text!.isEmpty {
            makeToast(strMessage: "Please_select_course_key".localized)
        } else if txtYearOfStudy.text!.isEmpty {
            makeToast(strMessage: "Please_select_year_of_study_key".localized)
        } else if (txtAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_address_key".localized)
        }
        else if !self.vwSpecifyCource.isHidden && (txtSpecifyCource.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_specify_course".localized)
        }///--vp
        else if !self.vwSpecifyYear.isHidden && (txtSpecifyYear.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_specify_year_of_study".localized)
        }///--vp
        else if (txtCity.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_city_key".localized)
        } else if (txtState.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_state_key".localized)
        } else if (txtPinCode.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_pincode_key".localized)
        } else if (txtUserName.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_username_key".localized)
        }  else if (txtEmailID.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage:"Please_enter_email_address_key".localized)
        } else if txtEmailID.text!.isValidEmail() == false {
            makeToast(strMessage: "Please_enter_valid_email_key".localized)
        } else if txtMobileNo.text!.isEmpty {
            makeToast(strMessage: "Please_enter_mobile_number_key".localized)
        } else if txtMobileNo.text!.count < mobileLength {
            makeToast(strMessage: "Please_enter_valid_mobile_number_key".localized)
        } else {
            var dict:[String:Any] = [:]
            dict["lang"] = lang
            dict["name"] = txtName.text?.trimmingCharacters(in: .whitespaces)
            dict["sex"] = btnMale.isSelected ? "Male" : "Female"
            dict["dob"] = self.txtDOB.text
            dict["institution"] = self.txtInstitution.text
            dict["name_of_institution"] = self.txtNameOfInstitution.text
            dict["course_name"] = self.txtCourse.text
            dict["year_of_study"] = self.txtYearOfStudy.text
            dict["address"] = self.txtAddress.text?.trimmingCharacters(in: .whitespaces)
            dict["city"] = self.txtCity.text?.trimmingCharacters(in: .whitespaces)
            dict["state"] = self.txtState.text?.trimmingCharacters(in: .whitespaces)
            dict["pincode"] = self.txtPinCode.text
            dict["user_name"] = self.txtUserName.text?.trimmingCharacters(in: .whitespaces)
            dict["email_id"] = self.txtEmailID.text
            dict["telephone"] = self.txtTelePhoneNo.text
            dict["mobile_no"] = self.txtMobileNo.text
            dict["timezone"] =  getCurrentTimeZone()
            dict["device_token"] = "122"
            dict["register_id"] =  "123"
            dict["device_type"] =  strDeviceType
            dict["user_id"] = getUserDetail("user_id")
            dict["access_token"] = getUserDetail("access_token")
            dict["flag_view"] = "1"
            
            ///--vp
            if self.txtCourse.text == "Other" {
                if !(txtSpecifyCource.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
                    dict["course_other"] =  txtSpecifyCource.text!
                }
            }
            if self.txtYearOfStudy.text == "Other" {
                if !(txtSpecifyYear.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
                    dict["study_other"] =  txtSpecifyYear.text!
                }
            }
            ///--
            
            print("dict \(dict)")
            
            regiViewModel.registerAPI(dict: dict, imageBonofide: self.imgBoafide.image!,isForUpdate: true, isPdf: isPdf, pdfData: pdfData) {
                self.handlerUpdateProfile()
                self.navigationController?.popViewController(animated: true)
                
            }
        }
    }
    
}

//MARK:- UITextfield Delegate

extension EditProfileVc:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtName || textField == txtCity || textField == txtState || textField == txtInitial || textField == txtNameOfInstitution   {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else if textField == txtMobileNo || textField == txtPinCode || textField == txtTelePhoneNo {
            let cs = NSCharacterSet(charactersIn: NUMERIC_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            if textField == txtMobileNo {
                let maxLength = mobileLength
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength && (string == filtered)
            }
            return (string == filtered)
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCourse{
            self.view.endEditing(true)
            let arrCourseName = regiViewModel.arrCourseData.map {$0.courseName}
            courseDD.dataSource = arrCourseName
            courseDD.show()
            courseDD.selectionAction = { (index: Int, item: String) in
                self.txtCourse.text = item
                self.setUpNewDataField()
            }
            return false
        } else if textField == txtYearOfStudy {
            self.view.endEditing(true)
            let arrStudyYear = regiViewModel.arrYearsStudy.map {$0.yearOfStudy}
            yearStudyDD.dataSource = arrStudyYear
            yearStudyDD.show()
            yearStudyDD.selectionAction = { (index: Int, item: String) in
                self.txtYearOfStudy.text = item
                self.setUpNewDataField()
            }
            return false
        } else if textField == txtDOB {
            self.view.endEditing(true)
            let obj = DatePickerVC()
            obj.isSetMaximumDate = true
            obj.isSetMinimumDate = false
            obj.isSetDate = true
            obj.maximumDate = Date()
            obj.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.date)
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            obj.handlerDateTime = {[weak self] date,type in
                let strDate = DateToString(Formatter: enumDateFormatter.dateFormatter1.rawValue, date: date)
                self?.txtDOB.text = strDate
            }
            self.present(obj, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

//MARK: - image selection


extension EditProfileVc : TOCropViewControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIDocumentPickerDelegate{

    func selectImages()
    {
        customImagePicker.typeOfPicker = .onlyPhoto
        
        customImagePicker.showImagePicker(fromViewController: self,
                                          navigationColor: UIColor.appThemeDarkBlueColor,
                                          imagePicked: { (response) in
                                            
                                            let theImage = response[UIImagePickerController.InfoKey.originalImage] as! UIImage

                                            self.openCropVC(Image: theImage)
                                            
        }, imageCanceled: {
        }, imageRemoved: nil)
    }
    
    func openCropVC(Image:UIImage)
    {
        let cropVC = TOCropViewController(image: Image)
        
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        //        cropVC.navigationController?.isNavigationBarHidden = true
        
        cropVC.customAspectRatio = CGSize(width: 4, height: 3)
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        
        self.present(cropVC, animated: false, completion: nil)
        
    }
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        
        isPdf = false
        pdfData = Data()
        imgBoafide.image = image
        cropViewController.dismiss(animated: false, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        // isHaveImage = false
        dismiss(animated: true, completion: nil)
    }
    
    func openActionSheetOption(){
        let alert = UIAlertController(title: "Choose_bonofide_certificate_key".localized.capitalized, message: "", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Document_key".localized, style: .default , handler:{ (UIAlertAction)in
            self.openDocumentFromiCloud()
        }))

        alert.addAction(UIAlertAction(title: "Camera_key".localized, style: .default , handler:{ (UIAlertAction)in
            self.openCamera()
            self.picker.delegate = self
        }))

        alert.addAction(UIAlertAction(title: "Choose_photo_key".localized, style: .default , handler:{ (UIAlertAction)in
            self.openGallery()
            self.picker.delegate = self
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel_key".localized, style: .cancel , handler:{ (UIAlertAction)in
            
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        } else {
            
        }
    }
    func openGallery(){
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
         picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        self.openCropVC(Image: image)
    }
    
    func openDocumentFromiCloud(){
        let types:[String] = [kUTTypePDF as String]
        let documentPicker = UIDocumentPickerViewController(documentTypes: types, in: .import)
        
        documentPicker.delegate = self
        self.present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        let fileName = url.lastPathComponent
        print("fileName:=",fileName)
        self.imgBoafide.image =  UIImage(named: "ic_pdf")
        isPdf = true
        pdfData = try! Data(contentsOf: url)
        /*let data = try! Data(contentsOf: url)
        let mimeType = MimeType(path: url.absoluteString).value
        uploadAttachment(type: MessageType.attachment.rawValue, data: data, mimeType: mimeType,name: fileName)*/
        controller.dismiss(animated: true, completion: nil)
    }
    
}

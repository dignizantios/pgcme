//
//  ForgotPasswordViewModel.swift
//  PGCME
//
//  Created by YASH on 16/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation

class ForgotPasswordViewModel {
    
    init() {
        
    }
}


extension ForgotPasswordViewModel{
    
    func forgotPasswordAPICalling(param:[String:Any],completion:@escaping()->Void){
        
        let url = kBaseURL + kForgotPassword
        
        CommonService().Service(url: url, param: param) { (response) in
            
            if let dictData = response.value{
                
                if dictData["flag"].stringValue == strSuccessResponse{
                    
                    makeToast(strMessage: dictData["msg"].stringValue)
                    completion()
                    
                }else if dictData["flag"].stringValue == strAccessDenied{
                   
                }else{
                    makeToast(strMessage: dictData["msg"].stringValue)
                }
            }
        }
        
    }
    
}


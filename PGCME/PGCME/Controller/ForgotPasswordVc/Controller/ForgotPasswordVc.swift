//
//  ForgotPasswordVc.swift
//  PGCME
//
//  Created by YASH on 23/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class ForgotPasswordVc: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblForgotPassword: UILabel!
    @IBOutlet weak var lblSubMsg: UILabel!
    @IBOutlet weak var lblEmailID: UILabel!
    @IBOutlet weak var txtEmailID: CustomTextField!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    //MARK: - Variable
    
    let viewModel = ForgotPasswordViewModel()

    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI(){
        
        self.view.backgroundColor = UIColor.appThemeSkyBlueColor
        lblForgotPassword.text = "Forgot_Password_key".localized
        lblForgotPassword.font = themeFont(size: 25, fontname: .bold)
        lblForgotPassword.textColor = UIColor.appThemeGrayColor
        
        lblSubMsg.text = "Forgot_password_sub_msg_key".localized
        lblSubMsg.font = themeFont(size: 14, fontname: .medium)
        lblSubMsg.textColor = UIColor.appThemeGrayColor
        
        lblEmailID.textColor = UIColor.appThemeGrayColor
        lblEmailID.font = themeFont(size: 14, fontname: .medium)
        lblEmailID.text = "Email_ID_key".localized
        
        txtEmailID.font = themeFont(size: 17, fontname: .medium)
        txtEmailID.textColor = UIColor.appThemeDarkBlueColor
        txtEmailID.placeholder = "Enter_here_key".localized
        
        btnSubmit.setupThemeButtonUI()
        btnSubmit.setTitle("Submit_key".localized, for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        transperentNavigationBar()
    }
    
}

//MARK: - IBAction

extension ForgotPasswordVc{
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        if (txtEmailID.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage:"Please_enter_email_address_key".localized)
        } else if txtEmailID.text!.isValidEmail() == false {
            makeToast(strMessage: "Please_enter_valid_email_key".localized)
        }else{
            
            let param = ["lang" : lang,
                         "email_id" : txtEmailID.text!.trimmingCharacters(in: .whitespaces),
                         "timezone" : getCurrentTimeZone()
                        ]
            
            viewModel.forgotPasswordAPICalling(param: param) { [weak self] in
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
}


//
//  RegisterViewModel.swift
//  PGCME
//
//  Created by Jaydeep on 14/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class RegisterViewModel {
    
    var arrCourseData:[CourseModel] = []
    var arrYearsStudy:[YearsStudyData] = []
    
    init() {
        getCourseData()
        getYeasOfStudyData()
    }
}


//MARK:- Service

extension RegisterViewModel {
    
    func getCourseData(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBaseURL)\(kGetCourse)"
            
            print("URL: \(url)")
            
            let param = ["lang" : lang,
                         "user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "timezone" : getCurrentTimeZone()]
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
               
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        if let dicData =  json.dictionaryObject , let courseList = CourseModelList(JSON: dicData) {
                             self.arrCourseData = courseList.list
                        }
                    }
                    else if json["flag"].stringValue == strAccessDenied
                    {
                        
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
        
    }
    
    func getYeasOfStudyData(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBaseURL)\(kGetStudyData)"
            
            print("URL: \(url)")
            
            let param = ["lang" : lang,
                         "user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "timezone" : getCurrentTimeZone()]
            
            print("param :\(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
               
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        if let dicData =  json.dictionaryObject , let yearStudyList = YersStudyList(JSON: dicData) {
                             self.arrYearsStudy = yearStudyList.list
                        }
                    }
                    else if json["flag"].stringValue == strAccessDenied
                    {
                        
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
        
    }
    
    
    func registerAPI(dict:[String:Any],imageBonofide:UIImage,isForUpdate:Bool = false,isPdf:Bool,pdfData:Data,completion:@escaping() -> Void){
          
        let url = isForUpdate ? "\(kBaseURL)\(kGetAndUpdateProfileDetails)" : "\(kBaseURL)\(kRegister)"        
        
        var pngName = "image.png"
        if isPdf {
            pngName = "bonafide.pdf"
        }
           
        CommonService().uploadImagesService(url: url, img: imageBonofide, withName: "bonafide", fileName: pngName,isPdf:isPdf, pdfData: pdfData, param: dict) { (respones) in
              
               if let json = respones.value
               {
                   hideLoader()
                   print("JSON : \(json)")
                   
                   if json["flag"].stringValue == strSuccessResponse
                   {
                        makeToast(strMessage: json["msg"].stringValue)
                        completion()
                   }
                   else if json["flag"].stringValue == strAccessDenied
                   {
                       
                   }
                   else
                   {
                       makeToast(strMessage: json["msg"].stringValue)
                   }
               }
               else
               {
                   hideLoader()
                   makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
               }
           }
       }
    
}

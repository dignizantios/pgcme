//
//  RegisterVc.swift
//  PGCME
//
//  Created by YASH on 23/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import TOCropViewController
import MobileCoreServices
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import DropDown

class RegisterVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var mainScroll: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var btnRegistration: CustomButton!
    @IBOutlet weak var btnSignIn: CustomButton!
    @IBOutlet weak var lblDR: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var lblInitial: UILabel!
    @IBOutlet weak var txtInitial: CustomTextField!
    @IBOutlet weak var btnMale: CustomButton!
    @IBOutlet weak var btnFemale: CustomButton!
    @IBOutlet weak var lblDateOfBirth: UILabel!
    @IBOutlet weak var txtDOB: CustomTextField!
    @IBOutlet weak var lblInstitute: UILabel!
    @IBOutlet weak var txtInstitute: CustomTextField!
    @IBOutlet weak var vwNameOfInstitute: UIView!
    @IBOutlet weak var lblNameOfInstitution: UILabel!
    @IBOutlet weak var txtNameOfInstitution: CustomTextField!
    @IBOutlet weak var lblCourse: UILabel!
    @IBOutlet weak var txtCourse: CustomTextField!
    @IBOutlet weak var lblYearOfStudy: UILabel!
    @IBOutlet weak var txtYearOfStudy: CustomTextField!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var txtAddress: CustomTextField!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var txtCity: CustomTextField!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var txtState: CustomTextField!
    @IBOutlet weak var lblPinCode: UILabel!
    @IBOutlet weak var txtPinCode: CustomTextField!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var txtUserName: CustomTextField!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var btnHideShowPassword: UIButton!
    @IBOutlet weak var lblReTypePassword: UILabel!
    @IBOutlet weak var btnHideShowReTypePassword: UIButton!
    @IBOutlet weak var txtRetypePassword: CustomTextField!
    @IBOutlet weak var lblEmailID: UILabel!
    @IBOutlet weak var txtEmailID: CustomTextField!
    @IBOutlet weak var lblTelephoneNo: UILabel!
    @IBOutlet weak var txtTelePhoneNo: CustomTextField!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var txtMobileNo: CustomTextField!
    @IBOutlet weak var imgBoafide: UIImageView!
    @IBOutlet weak var lblBonafied: UILabel!
    @IBOutlet weak var btnRegister: CustomButton!
    
    ///--New
    @IBOutlet weak var vwCourseDetails: UIView!
    @IBOutlet weak var vwSpecifyCource: UIView!
    @IBOutlet weak var lblSpecifyCource: UILabel!
    @IBOutlet weak var txtSpecifyCource: CustomTextField!
    
    @IBOutlet weak var vwSpecifyYear: UIView!
    @IBOutlet weak var lblSpecifyYear: UILabel!
    @IBOutlet weak var txtSpecifyYear: CustomTextField!
    
    @IBOutlet weak var vwCmeCrediteCerificate: UIView!
    @IBOutlet weak var lblCmeCrediteCerificate: UILabel!
    @IBOutlet weak var btnRequired: UIButton!
    @IBOutlet weak var btnNotRequired: UIButton!
    @IBOutlet weak var vwUploadBonafide: UIView!
    
    
    
    //MARK: - Variable
    
    private var customImagePicker = CustomImagePicker()
    var picker = UIImagePickerController()
    var courseDD = DropDown()
    var yearStudyDD = DropDown()
    var instituteDD = DropDown()
    let regiViewModel = RegisterViewModel()
    var isImageSelected = false
    var pdfData = Data()
    var isPdf = Bool()
    var arrayInstitute : [String] = []
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        self.arrayInstitute = ["Other", "CMC Vellore"]
    }

    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithSideMenuButton(titleText: "Registration_key".localized)
    }
    
    override func viewDidAppear(_ animated: Bool) {
                
    }
    
    override func viewDidLayoutSubviews() {
        self.configureDropdown(dropdown: courseDD, sender:txtCourse)
        self.configureDropdown(dropdown: yearStudyDD, sender:txtYearOfStudy,isWidth:false)
        self.configureDropdown(dropdown: instituteDD, sender: txtInstitute)
        
    }
    
    //MARK: - SetupUI
    
    func setupUI(){
        
        if dictSideMenu["is_register_available"] == "0"{
            mainScroll.isHidden = true
            let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 35))
            lbl.text = "Registration_closed_key".localized
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = .appThemeGrayColor
            lbl.font = themeFont(size: 20, fontname: .medium)
            lbl.center = self.view.center
            self.view.addSubview(lbl)
            return
        }
        
        self.view.backgroundColor = UIColor.appThemeSkyBlueColor
        
        imgBoafide.layer.cornerRadius = 5.0
        imgBoafide.layer.masksToBounds = true
        
        btnRegistration.titleLabel?.font = themeFont(size: 30, fontname: .bold)
        btnRegistration.setTitle("Registration_key".localized, for: .normal)
        btnRegistration.setTitleColor(UIColor.appThemeGrayColor, for: .normal)
        
        btnSignIn.titleLabel?.font = themeFont(size: 17, fontname: .medium)
        btnSignIn.setTitle("Sign_in_key".localized, for: .normal)
        btnSignIn.setTitleColor(UIColor.appThemeDarkBlueColor, for: .normal)
        [lblName,lblInitial,lblDateOfBirth,lblInstitute,lblNameOfInstitution,lblCourse,lblYearOfStudy,lblAddress,lblCity,lblState,lblPinCode,lblUserName,lblPassword,lblReTypePassword,lblEmailID,lblTelephoneNo,lblMobileNo,lblBonafied].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeGrayColor
            lbl?.font = themeFont(size: 14, fontname: .medium)
        }
        [txtName,txtInitial,txtDOB,txtInstitute,txtNameOfInstitution,txtCourse,txtYearOfStudy,txtAddress,txtCity,txtState,txtPinCode,txtUserName,txtPassword,txtRetypePassword,txtEmailID,txtTelePhoneNo,txtMobileNo].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.textColor = UIColor.appThemeDarkBlueColor
            txt?.placeholder = "Enter_here_key".localized
            txt?.delegate = self
        }
        
        self.txtInstitute.text = "Other"
//        self.vwNameOfInstitute.isHidden = true
        
        [txtTelePhoneNo,txtPinCode,txtMobileNo].forEach { (txtField) in
            addDoneButtonOnKeyboard(textfield: txtField!)
            txtField?.keyboardType = .phonePad
        }
        
        txtCourse.placeholder = "Select_here_key".localized
        txtYearOfStudy.placeholder = "Select_here_key".localized
        
        lblDR.textColor = UIColor.appThemeGrayColor
        lblDR.font = themeFont(size: 16, fontname: .medium)
        
        lblDR.text = "Dr_key".localized
        lblName.text = "Name_key".localized
        lblInitial.text = "Initial_key".localized
        lblDateOfBirth.text = "Date_of_Birth_key".localized
        lblInstitute.text = "Institution_key".localized
        lblNameOfInstitution.text = "Name_of_Institution_key".localized
        lblCourse.text = "Course_key".localized
        lblYearOfStudy.text = "Year_of_Study_key".localized
        lblAddress.text = "Address_key".localized
        lblCity.text = "City_key".localized
        lblState.text = "State_key".localized
        lblPinCode.text = "Pincode_key".localized
        lblUserName.text = "Username_key".localized
        lblPassword.text = "Password_key".localized
        lblReTypePassword.text = "Re-type_Password_key".localized
        lblEmailID.text = "Email_ID_key".localized
        lblTelephoneNo.text = "Telephone_No_key".localized
        lblMobileNo.text = "Mobile_No_key".localized
        lblBonafied.text = "Upload_Bonafide_Certificate_key".localized
        btnRegister.setTitle("Register_key".localized, for: .normal)
        btnRegister.setupThemeButtonUI()
        
        [btnMale,btnFemale].forEach { (btn) in
            btn?.setTitleColor(UIColor.appThemeGrayColor, for: .normal)
            btn?.borderColor = UIColor.appThemeGrayColor
        }
        
        [lblSpecifyCource, lblSpecifyYear, lblCmeCrediteCerificate].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeGrayColor
            lbl?.font = themeFont(size: 14, fontname: .medium)
        }
        
        [txtSpecifyYear, txtSpecifyCource].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.textColor = UIColor.appThemeDarkBlueColor
            txt?.placeholder = "Enter_here_key".localized
            txt?.delegate = self
        }
        
        [btnRequired, btnNotRequired].forEach { (btn) in
            btn?.setTitleColor(UIColor.appThemeDarkBlueColor, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .light)
        }
        
        btnRequired.isSelected = true
        btnNotRequired.isSelected = false
        
        if self.txtInstitute.text == "Other" {
            self.vwCmeCrediteCerificate.isHidden = false
        }
        self.vwCourseDetails.isHidden = true
        [vwSpecifyYear, vwSpecifyCource].forEach { (vw) in
            vw?.isHidden = true
        }
    }
    
    ///--vp
    func setUpNewDataField() {
        
        self.vwCmeCrediteCerificate.isHidden = true
        
        if self.txtInstitute.text == "Other" {
            self.vwCmeCrediteCerificate.isHidden = false
            btnRequired.isSelected = true
            btnNotRequired.isSelected = false
        }
        else {
            self.vwUploadBonafide.isHidden = false
            btnRequired.isSelected = true
            btnNotRequired.isSelected = false
        }
        
        self.vwCourseDetails.isHidden = true
        [vwSpecifyCource, vwSpecifyYear].forEach { (vw) in
            vw?.isHidden = true
        }
        if self.txtCourse.text == "Other" || self.txtYearOfStudy.text == "Other" {
            self.vwCourseDetails.isHidden = false
            
            if self.txtCourse.text == "Other" {
                self.vwSpecifyCource.isHidden = false
            }
            else {
               self.txtSpecifyCource.text = ""
            }
            if self.txtYearOfStudy.text == "Other" {
                self.vwSpecifyYear.isHidden = false
            }
            else {
               self.txtSpecifyYear.text = ""
            }
        }
        else {
            self.txtSpecifyYear.text = ""
            self.txtSpecifyCource.text = ""
        }
    }
}

//MARK: - IBAction method

extension RegisterVc{
    
    
    func btnSelectUnSelect(selectedBtn:CustomButton,unSelectedBtn:CustomButton){
        
        selectedBtn.isSelected = !selectedBtn.isSelected
        selectedBtn.borderColor = UIColor.appThemeDarkBlueColor
        selectedBtn.setTitleColor(selectedBtn.isSelected == true ? UIColor.appThemeDarkBlueColor : UIColor.appThemeGrayColor, for: .normal)
        
        unSelectedBtn.isSelected = false
        unSelectedBtn.borderColor = UIColor.appThemeGrayColor
        unSelectedBtn.setTitleColor(UIColor.appThemeGrayColor, for: .normal)
        
    }
    
    @IBAction func btnMaleTapped(_ sender: UIButton) {
        btnSelectUnSelect(selectedBtn: btnMale, unSelectedBtn: btnFemale)
    }
    
    @IBAction func btnFemaleTapped(_ sender: UIButton) {
        btnSelectUnSelect(selectedBtn: btnFemale, unSelectedBtn: btnMale)
    }
    
    @IBAction func btnUploadBonafideTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        openActionSheetOption()
    }
    
    @IBAction func btnRegisterTapped(_ sender: UIButton) {
        
        if (txtName.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_name_key".localized)
        } else if (txtInitial.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_initial_name_key".localized)
        } else if self.btnMale.isSelected == false && self.btnFemale.isSelected == false{
            makeToast(strMessage: "Please_select_gender_key".localized)
        } else if txtDOB.text!.isEmpty {
            makeToast(strMessage: "Please_select_date_of_birth_key".localized)
        } else if (txtInstitute.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_institution_name_key".localized)
        } else if (txtInstitute.text != "CMC Vellore") && (txtNameOfInstitution.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_name_of_institution_name_key".localized)
        } else if txtCourse.text!.isEmpty {
            makeToast(strMessage: "Please_select_course_key".localized)
        } else if txtYearOfStudy.text!.isEmpty {
            makeToast(strMessage: "Please_select_year_of_study_key".localized)
        }
        else if !self.vwSpecifyCource.isHidden && (txtSpecifyCource.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_specify_course".localized)
        }///--vp
        else if !self.vwSpecifyYear.isHidden && (txtSpecifyYear.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_specify_year_of_study".localized)
        }///--vp
        else if (txtAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_address_key".localized)
        } else if (txtCity.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_city_key".localized)
        } else if (txtState.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_state_key".localized)
        } else if (txtPinCode.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_pincode_key".localized)
        } else if (txtUserName.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: "Please_enter_username_key".localized)
        } else if (txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage:"Please_enter_password_key".localized)
        } else if (txtPassword.text?.count)! < passwordLegth {
            makeToast(strMessage: "Password_must_be_at_least_eight_characters_long_key".localized)
        } /*else if (txtRetypePassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: "Please_enter_confirm_password_key".localized)
        }*/ else if(!(txtPassword.text! == txtRetypePassword.text!)) {
            makeToast(strMessage:  "Password_and_confirmation_password_must_match_key".localized)
        } else if (txtEmailID.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage:"Please_enter_email_address_key".localized)
        } else if txtEmailID.text!.isValidEmail() == false {
            makeToast(strMessage: "Please_enter_valid_email_key".localized)
        } else if txtMobileNo.text!.isEmpty {
            makeToast(strMessage: "Please_enter_mobile_number_key".localized)
        } else if txtMobileNo.text!.count < mobileLength {
            makeToast(strMessage: "Please_enter_valid_mobile_number_key".localized)
        } else if isImageSelected == false && isPdf == false && btnNotRequired.isSelected == false {
//            btnNotRequired.isSelected == false {
            makeToast(strMessage: "Please_select_certificate_key".localized)
        } else {
            var dict:[String:Any] = [:]
            dict["lang"] = lang
            dict["name"] = txtName.text?.trimmingCharacters(in: .whitespaces)
            dict["initial"] = txtInitial.text?.trimmingCharacters(in: .whitespaces)
            dict["sex"] = btnMale.isSelected ? "Male" : "Female"
            dict["dob"] = self.txtDOB.text
            dict["name_of_institution"] = self.txtNameOfInstitution.text
            dict["institution"] = self.txtInstitute.text
            dict["course_name"] = self.txtCourse.text
            dict["year_of_study"] = self.txtYearOfStudy.text
            dict["address"] = self.txtAddress.text?.trimmingCharacters(in: .whitespaces)
            dict["city"] = self.txtCity.text?.trimmingCharacters(in: .whitespaces)
            dict["state"] = self.txtState.text?.trimmingCharacters(in: .whitespaces)
            dict["pincode"] = self.txtPinCode.text
            dict["user_name"] = self.txtUserName.text?.trimmingCharacters(in: .whitespaces)
            dict["password"] = self.txtPassword.text
            dict["re_password"] = self.txtRetypePassword.text
            dict["email_id"] = self.txtEmailID.text
            dict["telephone"] = self.txtTelePhoneNo.text
            dict["mobile_no"] = self.txtMobileNo.text
            dict["timezone"] =  getCurrentTimeZone()
            dict["device_token"] = "122"
            dict["register_id"] =  "123"
            dict["device_type"] =  strDeviceType
            
            ///--vp
            if self.txtCourse.text == "Other" {
                if !(txtSpecifyCource.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
                    dict["course_other"] =  txtSpecifyCource.text!
                }
            }
            if self.txtYearOfStudy.text == "Other" {
                if !(txtSpecifyYear.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
                    dict["study_other"] =  txtSpecifyYear.text!
                }
            }
            if self.txtInstitute.text == "Other" {
                if btnRequired.isSelected {
                    dict["credit"] = "Required"
                }
                else if btnNotRequired.isSelected {
                    dict["credit"] = "Not Required"
                }
            }
            ///--
            
            print("dict \(dict)")
            
            regiViewModel.registerAPI(dict: dict, imageBonofide: self.imgBoafide.image!, isPdf: isPdf, pdfData: pdfData) {
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigateToSideMenuSelectedController(obj: obj)
            }
        }
    }

    @IBAction func btnSignInTapped(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        self.navigateToSideMenuSelectedController(obj: obj)
    }
    
    @IBAction func btnHideShowPasswordTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        txtPassword.isSecureTextEntry = sender.isSelected ? false : true
    }
    
    @IBAction func btnHideShowReTypePasswordTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        txtRetypePassword.isSecureTextEntry = sender.isSelected ? false : true
    }

    @IBAction func btnRequiredAction(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            btnNotRequired.isSelected = true
        } else {
            btnNotRequired.isSelected = false
            sender.isSelected = true
            self.vwUploadBonafide.isHidden = false
        }
    }
    
    @IBAction func btnNotRequiredAction(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            btnRequired.isSelected = true
        } else {
            btnRequired.isSelected = false
            sender.isSelected = true
            self.vwUploadBonafide.isHidden = true
        }
    }
    
}

//MARK:- UITextfield Delegate

extension RegisterVc:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtName || textField == txtCity || textField == txtState || textField == txtInitial || textField == txtNameOfInstitution   {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else if textField == txtMobileNo || textField == txtPinCode || textField == txtTelePhoneNo {
            let cs = NSCharacterSet(charactersIn: NUMERIC_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            if textField == txtMobileNo {
                let maxLength = mobileLength
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength && (string == filtered)
            }
            return (string == filtered)
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCourse{
            self.view.endEditing(true)
            let arrCourseName = regiViewModel.arrCourseData.map {$0.courseName}
            courseDD.dataSource = arrCourseName
            courseDD.show()
            courseDD.selectionAction = { (index: Int, item: String) in
                self.txtCourse.text = item
                self.setUpNewDataField() ///--vp
            }
            return false
        } else if textField == txtYearOfStudy {
            self.view.endEditing(true)
            let arrStudyYear = regiViewModel.arrYearsStudy.map {$0.yearOfStudy}
            yearStudyDD.dataSource = arrStudyYear
            yearStudyDD.show()
            yearStudyDD.selectionAction = { (index: Int, item: String) in
                self.txtYearOfStudy.text = item
                self.setUpNewDataField() ///--vp
            }
            return false
        }else if textField == txtInstitute {
            self.view.endEditing(true)
            self.instituteDD.dataSource = arrayInstitute
            instituteDD.show()
            instituteDD.selectionAction = {(index:Int, item: String) in
                self.txtInstitute.text = item
                
                if item == "CMC Vellore" {
                    self.vwNameOfInstitute.isHidden = true
                }
                else {
                    self.vwNameOfInstitute.isHidden = false
                }
                self.setUpNewDataField() ///--vp
            }
            return false
        }
        else if textField == txtDOB {
            self.view.endEditing(true)
            let obj = DatePickerVC()
            obj.isSetMaximumDate = true
            obj.isSetMinimumDate = false
            obj.isSetDate = true
            obj.maximumDate = Date()
            obj.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.date)
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            obj.handlerDateTime = {[weak self] date,type in
                let strDate = DateToString(Formatter: enumDateFormatter.dateFormatter1.rawValue, date: date)
                self?.txtDOB.text = strDate
            }
            self.present(obj, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

//MARK: - image selection

extension RegisterVc : TOCropViewControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIDocumentPickerDelegate{

    func selectImages()
    {
        customImagePicker.typeOfPicker = .onlyPhoto
        
        customImagePicker.showImagePicker(fromViewController: self,
                                          navigationColor: UIColor.appThemeDarkBlueColor,
                                          imagePicked: { (response) in
                                            
                                            let theImage = response[UIImagePickerController.InfoKey.originalImage] as! UIImage

                                            self.openCropVC(Image: theImage)
                                            
        }, imageCanceled: {
        }, imageRemoved: nil)
    }
    
    func openCropVC(Image:UIImage)
    {
        let cropVC = TOCropViewController(image: Image)
        
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        //        cropVC.navigationController?.isNavigationBarHidden = true
        
        cropVC.customAspectRatio = CGSize(width: 4, height: 3)
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        
        self.present(cropVC, animated: false, completion: nil)
        
    }
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        isImageSelected = true
        isPdf = false
        pdfData = Data()
        imgBoafide.image = image
        cropViewController.dismiss(animated: false, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        // isHaveImage = false
        dismiss(animated: true, completion: nil)
    }
    
    func openActionSheetOption(){
        let alert = UIAlertController(title: "Choose_bonofide_certificate_key".localized.capitalized, message: "", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Document_key".localized, style: .default , handler:{ (UIAlertAction)in
            self.openDocumentFromiCloud()
        }))

        alert.addAction(UIAlertAction(title: "Camera_key".localized, style: .default , handler:{ (UIAlertAction)in
            self.openCamera()
            self.picker.delegate = self
        }))

        alert.addAction(UIAlertAction(title: "Choose_photo_key".localized, style: .default , handler:{ (UIAlertAction)in
            self.openGallery()
            self.picker.delegate = self
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel_key".localized, style: .cancel , handler:{ (UIAlertAction)in
            
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        } else {
            
        }
    }
    func openGallery(){
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
         picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        self.openCropVC(Image: image)
    }
    
    func openDocumentFromiCloud(){
        let types:[String] = [kUTTypePDF as String]
        let documentPicker = UIDocumentPickerViewController(documentTypes: types, in: .import)
        
        documentPicker.delegate = self
        self.present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        isImageSelected = true
        let fileName = url.lastPathComponent
        print("fileName:=",fileName)
        self.imgBoafide.image =  UIImage(named: "ic_pdf")
        isImageSelected = false
        isPdf = true
        pdfData = try! Data(contentsOf: url)
        /*let data = try! Data(contentsOf: url)
        let mimeType = MimeType(path: url.absoluteString).value
        uploadAttachment(type: MessageType.attachment.rawValue, data: data, mimeType: mimeType,name: fileName)*/
        controller.dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- Service

extension RegisterVc {

   
}


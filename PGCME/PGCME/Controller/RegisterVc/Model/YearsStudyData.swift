//
//  YearsStudyData.swift
//  PGCME
//
//  Created by Jaydeep on 16/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import ObjectMapper

class YersStudyList: Mappable {
    
    var list =  [YearsStudyData]()
    
    required convenience init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}

class YearsStudyData: Mappable {

    var yearId: String = ""
    var yearOfStudy: String = ""

    required convenience init?(map: Map){
        self.init()
    }

    func mapping(map: Map) {
        yearId <- map["year_id"]
        yearOfStudy <- map["year_of_study"]
    }
}

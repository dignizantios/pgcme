//
//  CourseModel.swift
//  PGCME
//
//  Created by Jaydeep on 14/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import ObjectMapper

class CourseModelList: Mappable {
    
    var list =  [CourseModel]()
    
    required convenience init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}

class CourseModel: Mappable {

    var courseId: String = ""
    var courseName: String = ""

    required convenience init?(map: Map){
        self.init()
    }

    func mapping(map: Map) {
        courseId <- map["course_id"]
        courseName <- map["course_name"]
    }
}

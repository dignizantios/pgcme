import Foundation 
import ObjectMapper 

class EducationMaterialModel: Mappable { 

	var list = [EducationaMaterialData]()

    required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		list <- map["data"]
	}
} 

class EducationaMaterialData: Mappable {

	var doctorName: String = ""
	var material = [Material]()

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		doctorName <- map["doctor_name"] 
		material <- map["material"] 
	}
} 

class Material: Mappable { 

	var materialId: String = ""
	var title: String = ""
	var fileAttachment: String = ""

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		materialId <- map["material_id"] 
		title <- map["title"] 
		fileAttachment <- map["file_attachment"] 
	}
} 


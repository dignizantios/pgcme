//
//  QuestionaryHeaderCell.swift
//  PGCME
//
//  Created by YASH on 26/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import TGPControls


class QuestionaryHeaderCell: UITableViewCell {

    //MARK: - Outlet
    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var txtViewQuestion: UITextView!
//    @IBOutlet weak var lblMainHeader: UILabel!
    @IBOutlet weak var tblSubData: UITableView!
    @IBOutlet weak var constantTblSubHeight: NSLayoutConstraint!
    @IBOutlet weak var lblNoSlider: TGPCamelLabels!
    @IBOutlet weak var sliderValue: TGPDiscreteSlider!
    @IBOutlet weak var vwSlider: UIView!
    @IBOutlet weak var vwSliderHeight: NSLayoutConstraint!
    var hanlorSliderValue:(Int)->Void = { _ in }
    
    
    private func localizedStrings(_ key: String) -> [String] {
        return NSLocalizedString(key, comment: "")
            .split(separator: " ")
            .map({ (substring) -> String in
                return "\(substring)"
            })
    }

    
    //MARK: - View life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblIndex.textColor = UIColor.black
        lblIndex.font = themeFont(size: 16, fontname: .medium)
        
        txtViewQuestion.textColor = UIColor.black
        txtViewQuestion.font = themeFont(size: 16, fontname: .medium)
        
        tblSubData.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
        lblNoSlider.names = localizedStrings("oneTo10Labels.numbers")
        sliderValue.ticksListener = lblNoSlider
        
        sliderValue.addTarget(self, action: #selector(valueChanged(_:event:)), for: .valueChanged)
        
        self.vwSlider.isHidden = true
        self.vwSliderHeight.constant = 0
    }
    
    override func draw(_ rect: CGRect) {
        self.contentView.layoutIfNeeded()
        self.contentView.layoutSubviews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func valueChanged(_ sender: TGPDiscreteSlider, event:UIEvent) {
        self.hanlorSliderValue(Int(sender.value))
        print("VALUE: ", Int(sender.value))
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
           // print("contentSize:= \(tblSubData.contentSize.height)")
            self.contentView.layoutIfNeeded()
            self.contentView.layoutSubviews()
            self.constantTblSubHeight.constant = tblSubData.contentSize.height
            }            
        }
    
}

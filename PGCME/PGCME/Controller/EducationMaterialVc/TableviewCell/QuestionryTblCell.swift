//
//  QuestionryTblCell.swift
//  PGCME
//
//  Created by YASH on 25/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class QuestionryTblCell: UITableViewCell {

    //MARK: - IBOutlet
    
    @IBOutlet weak var txtViewSubName: UITextView!
//    @IBOutlet weak var lblSubName: UILabel!
    @IBOutlet weak var vwRadio: UIView!
    @IBOutlet weak var btnRadio: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        txtViewSubName.textColor = UIColor.appThemeDarkBlueColor
        txtViewSubName.font = themeFont(size: 15, fontname: .medium)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

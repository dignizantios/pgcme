//
//  EducationMaterialViewModel.swift
//  PGCME
//
//  Created by YASH on 16/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation
class EducationMaterialViewModel  {
    
    
    var arrayEducationData = [EducationaMaterialData]()
    
    var completionHandlerSuccess:() -> Void = {}
    var completionHandlerFailure:(_ msg:String) -> Void = {_ in}
    
    //MARK: - View life cycle
    init() {
        getEducationMaterialList(isShowLoader:true)
    }
    
}

extension EducationMaterialViewModel{
    
    func getEducationMaterialList(isShowLoader:Bool){
        
        let url = kBaseURL + kEducationMaterial
        let param = ["lang" : lang,
                     "timezone" : getCurrentTimeZone(),
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token")
                    ]
        
        CommonService().Service(url: url, param: param,isShowLoader: isShowLoader) { (respones) in
            
            if let json = respones.value
            {
                print("JSON : \(json)")
                
                if json["flag"].stringValue == strSuccessResponse
                {
                    if let dicData =  json.dictionaryObject , let materialList = EducationMaterialModel(JSON:dicData) {
                        self.arrayEducationData = materialList.list
                        self.completionHandlerSuccess()
                    }
                }
                else if json["flag"].stringValue == strAccessDenied
                {
                    self.completionHandlerFailure(strAccessDenied)
                }
                else
                {
                    self.arrayEducationData = []
                    self.completionHandlerFailure(json["msg"].stringValue)
                }
            }
            else
            {
                makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
            }
        }
    }
}


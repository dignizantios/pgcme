//
//  EducationMaterialVc.swift
//  PGCME
//
//  Created by YASH on 25/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class EducationMaterialVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variable
    
    var viewModel = EducationMaterialViewModel()
    var strErrorMsg = ""
    var upperReferesh = UIRefreshControl()

    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        viewModel.completionHandlerSuccess = {[weak self] in
            self?.upperReferesh.endRefreshing()
            self?.tableView.reloadData()
        }
        
        viewModel.completionHandlerFailure = {[weak self] (errorMsg) in
            if errorMsg == strAccessDenied {
                self?.logoutAPICalling()
            } else {
                self?.upperReferesh.endRefreshing()
                self?.strErrorMsg = errorMsg
                self?.tableView.reloadData()
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithSideMenuButton(titleText: "Education_Materials_key".localized)
    }
    
    func setupUI(){
        
        self.view.backgroundColor = UIColor.appThemeSkyBlueColor
        
        self.tableView.register(UINib(nibName: "QuestionaryHeaderCell", bundle: nil), forCellReuseIdentifier: "QuestionaryHeaderCell")
        
        self.tableView.tableFooterView = UIView()
        
        upperReferesh.tintColor = UIColor.appThemeDarkBlueColor
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        
        tableView.addSubview(upperReferesh)

    }
    
    override func viewDidLayoutSubviews() {
        self.tableView.reloadData()
    }
    
    @objc func upperRefreshTable(){
        viewModel.getEducationMaterialList(isShowLoader:false)
    }

}

//MARK: - TableView Delegate

extension EducationMaterialVc: UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tableView{
            
            if viewModel.arrayEducationData.count == 0{
                
                let lbl = UILabel()
                lbl.text = self.strErrorMsg
                lbl.font = themeFont(size: 16, fontname: .medium)
                lbl.textAlignment = NSTextAlignment.center
                lbl.textColor = UIColor.appThemeGrayColor
                lbl.center = tableView.center
                tableView.backgroundView = lbl
                return 0
            }
            
            return viewModel.arrayEducationData.count
        }
        
        return viewModel.arrayEducationData[tableView.tag].material.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tableView{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionaryHeaderCell") as! QuestionaryHeaderCell
            
            let dict = viewModel.arrayEducationData[indexPath.row]
            cell.lblIndex.text = ""
            cell.txtViewQuestion.text = dict.doctorName
            cell.tblSubData.register(UINib(nibName: "QuestionryTblCell", bundle: nil), forCellReuseIdentifier: "QuestionryTblCell")
            cell.tblSubData.tag = indexPath.row
            cell.tblSubData.delegate = self
            cell.tblSubData.dataSource = self
            cell.tblSubData.reloadData()
            cell.tblSubData.layoutIfNeeded()
            cell.tblSubData.layoutSubviews()
            
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionryTblCell") as! QuestionryTblCell
        
        cell.vwRadio.isHidden = true
        
        let dict = viewModel.arrayEducationData[tableView.tag].material[indexPath.row]
        cell.txtViewSubName.text = dict.title
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.tableView{
            
            return
        }
        
        let dict = viewModel.arrayEducationData[tableView.tag].material[indexPath.row]
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WebDetailsVc") as! WebDetailsVc
        obj.selectedController = .educationMaterial
        obj.strURL = dict.fileAttachment
        obj.strTitle = dict.title
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

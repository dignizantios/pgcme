//
//  EmailVerificationViewModel.swift
//  PGCME
//
//  Created by Jaydeep on 28/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation

class  EmailVerificationViewModel {
    
    //MARK: - View life cycle
    
    init() {
       
    }
}

extension EmailVerificationViewModel{
    
    func postEmailVerification(param:[String:Any],completion:@escaping(String,String) -> Void){
        
        let url = kBaseURL + kPostEmailVerification
        
        CommonService().Service(url: url, param: param) { (respones) in
            
            if let json = respones.value
            {
                print("JSON : \(json)")
                
                if json["flag"].stringValue == strSuccessResponse
                {
                    completion("", json["msg"].stringValue)
                    makeToast(strMessage: json["msg"].stringValue)
                }
                else if json["flag"].stringValue == strAccessDenied
                {
                    completion(strAccessDenied, "")
                } else if json["flag"].stringValue == "3"{
                    completion(json["flag"].stringValue, json["msg"].stringValue)
                }
                else
                {
                    completion(json["msg"].stringValue, "")
                    makeToast(strMessage: json["msg"].stringValue)
                }
            }
            else
            {
                completion("Server_not_responding_Please_try_again_later_key".localized, "")
                makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
            }
        }
    }
}

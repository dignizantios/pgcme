//
//  EmailVerificationVc.swift
//  PGCME
//
//  Created by YASH on 28/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class EmailVerificationVc: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblEmailVerification: UILabel!
    @IBOutlet weak var lblSubMsg: UILabel!
    @IBOutlet weak var lblEmailID: UILabel!
    @IBOutlet weak var txtEmailID: CustomTextField!
    @IBOutlet weak var btnSubmit: CustomButton!

    //MARK: - Variable
    
     var emailViewModel = EmailVerificationViewModel()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        transperentNavigationBar()
    }
    
    //MARK: - Setup UI
    func setupUI(){
        
        self.view.backgroundColor = UIColor.appThemeSkyBlueColor
        lblEmailVerification.text = "Email_Verification_key".localized
        lblEmailVerification.font = themeFont(size: 25, fontname: .bold)
        lblEmailVerification.textColor = UIColor.appThemeGrayColor
        
        lblSubMsg.text = ""
//        lblSubMsg.text = "Forgot_password_sub_msg_key".localized
//        lblSubMsg.font = themeFont(size: 14, fontname: .medium)
//        lblSubMsg.textColor = UIColor.appThemeGrayColor
        
        lblEmailID.textColor = UIColor.appThemeGrayColor
        lblEmailID.font = themeFont(size: 14, fontname: .medium)
        lblEmailID.text = "Email_ID_key".localized
        
        txtEmailID.font = themeFont(size: 17, fontname: .medium)
        txtEmailID.textColor = UIColor.appThemeDarkBlueColor
        txtEmailID.placeholder = "Enter_here_key".localized
        
        btnSubmit.setupThemeButtonUI()
        btnSubmit.setTitle("Submit_key".localized, for: .normal)
        
    }

}

//MARK: - IBAction

extension EmailVerificationVc{
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        if (txtEmailID.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage:"Please_enter_email_address_key".localized)
        } else if txtEmailID.text!.isValidEmail() == false {
            makeToast(strMessage: "Please_enter_valid_email_key".localized)
        } else {
            let param = ["email_id" : self.txtEmailID.text ?? "",
                         "access_token":getUserDetail("access_token"),
                         "user_id":getUserDetail("user_id"),
                         "lang": lang,
                         "timezone" : getCurrentTimeZone(),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                         "register_id" : "45",
                         "device_type" : strDeviceType]
            
            emailViewModel.postEmailVerification(param: param) { (error, msg) in
                print("error\(error), msg \(msg)")
            }
        }
        
    }
}

import Foundation 
import ObjectMapper 

class HomeDataModel: Mappable { 

	var allDataURL: AllDataURL? 

    required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		allDataURL <- map["data"]
	}
} 

class AllDataURL: Mappable { 

	var introduction: String = ""
	var programDetails: String = ""
	var accomodation: String = ""
	var faculties: String = ""
	var registrationInstruction: String = ""
	var contact: String = ""
    var HowWillBenefit: String = ""
    var TopicsCovered : String = ""
    var scrollText : String = ""
    
    

    required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
        scrollText <- map["scroll_text"]
		introduction <- map["introduction"] 
		programDetails <- map["program_details"] 
		accomodation <- map["accomodation"] 
		faculties <- map["faculties"] 
		registrationInstruction <- map["registration_instruction"] 
		contact <- map["contact"]
        HowWillBenefit <- map["how_will_this_benefit"]
        TopicsCovered <- map["topics"]
	}
} 


//
//  ViewController.swift
//  PGCME
//
//  Created by YASH on 21/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import MarqueeLabel

class HomeViewController: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblMarquee: MarqueeLabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Variable
    
    var array : [JSON] = []
    var dictHomeDataURL = AllDataURL()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        self.setArray()
        
        if getUserDetail("access_token") == ""{
            guestUserLogin(completion: {
                self.checkSidemenuFlag {
                    self.homeDataAPICalling {
                        self.setUpMarqueeLabel(text: self.dictHomeDataURL.scrollText)
                    }
                }
            })
        } else {
            checkSidemenuFlag {
                print("Called")
                self.homeDataAPICalling {
                    self.setUpMarqueeLabel(text: self.dictHomeDataURL.scrollText)
                }
            }
        }
//        self.setUpMarqueeLabel()
        NotificationCenter.default.addObserver(self, selector: #selector(logoutAPICalling), name: .didLogoutUser, object: nil)
        
        if isDeepLink {
            isDeepLink = false
            setupNavigationbarwithSideMenuButton(titleText: "PG_CME_key".localized)
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
            self.navigateToSideMenuSelectedController(obj: obj)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithSideMenuButton(titleText: "PG_CME_key".localized)
    }

    func setupUI(){
        collectionView.register(UINib(nibName: "HomeCollectionCell", bundle: nil), forCellWithReuseIdentifier:"HomeCollectionCell")
        lblMarquee.text = ""
        self.view.backgroundColor = UIColor.appThemeSkyBlueColor
    }

    //MARK: Marquee Label
    func setUpMarqueeLabel(text: String) {
       
        self.lblMarquee.tag = 101
        self.lblMarquee.type = .continuous
        self.lblMarquee.speed = .duration(12)
        self.lblMarquee.animationCurve = .easeOut
        self.lblMarquee.fadeLength = 10.0
        self.lblMarquee.leadingBuffer = 30.0
        self.lblMarquee.trailingBuffer = 20.0
        self.lblMarquee.textColor = UIColor(displayP3Red: 209/255, green: 153/255, blue: 52/255, alpha: 1.0)
        self.lblMarquee.font = themeFont(size: 22, fontname: .medium)
        self.lblMarquee.text = text
    }
    
    //MARK: - Array

    func setArray(){
        /*
        1st row - > Introduction, Who & How will this benefit ?
        2nd row - > Topic to be coverd, Faculties
        3rd row - > Program details, Accomodation
        4th row - > Registration Instructions, Contact
        */
        
        var dict = JSON()
        dict["img"] = "ic_introduction"
        dict["name"] = "Introduction"
        self.array.append(dict)
        
        dict = JSON()
        dict["img"] = "ic_benefit"
        dict["name"] = "Who & How will this benefit ?"
        self.array.append(dict)
        
        dict = JSON()
        dict["img"] = "ic_covered"
        dict["name"] = "Topic to be covered"
        self.array.append(dict)
        
        dict = JSON()
        dict["img"] = "ic_faculties"
        dict["name"] = "Faculties"
        self.array.append(dict)
        
        dict = JSON()
        dict["img"] = "ic_program_details"
        dict["name"] = "Program\nDetails"
        self.array.append(dict)
        
        dict = JSON()
        dict["img"] = "ic_accomodation"
        dict["name"] = "Accomodation"
        self.array.append(dict)
        
        dict = JSON()
        dict["img"] = "ic_registration_instructions"
        dict["name"] = "Registration\nInstructions"
        self.array.append(dict)
        
        dict = JSON()
        dict["img"] = "ic_contact"
        dict["name"] = "Contact"
        self.array.append(dict)
        
        self.collectionView.reloadData()
        
    }
}



extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionCell", for: indexPath) as! HomeCollectionCell
        
        let dict = self.array[indexPath.row]
        
        cell.img.image = UIImage(named: dict["img"].stringValue)
        cell.lblName.text = dict["name"].stringValue
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if dictHomeDataURL.introduction == "" {
            return
        }
        
        print("URL : \(redirectWithDataURL(index:indexPath.row))")
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WebDetailsVc") as! WebDetailsVc
        obj.selectedController = .home
        obj.strTitle = array[indexPath.row]["name"].stringValue
        obj.strURL = redirectWithDataURL(index:indexPath.row)
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let topVwHeight = self.lblMarquee.frame.size.height+self.imgLogo.frame.size.height+15
//        let height = (self.view.frame.size.height-topVwHeight)/4
        let height = (self.view.frame.size.height-topVwHeight)/4+15
        
        let width = (collectionView.frame.size.width/2)
        return CGSize(width: width , height:height)
        
    }
    
    func redirectWithDataURL(index:Int) -> String{
        if index == 0{
            return dictHomeDataURL.introduction
        }
        else if index == 1 {
            return dictHomeDataURL.HowWillBenefit
//            return dictHomeDataURL.programDetails
        }
        else if index == 2 {
            return dictHomeDataURL.TopicsCovered
//            return dictHomeDataURL.accomodation
        }
        else if index == 3 {
            return dictHomeDataURL.faculties
        }
        else if index == 4 {
            return dictHomeDataURL.programDetails
//            return dictHomeDataURL.registrationInstruction
        }
        else if index == 5 {
            return dictHomeDataURL.accomodation
//            return dictHomeDataURL.contact
        }
        else if index == 6 {
            return dictHomeDataURL.registrationInstruction
//            return dictHomeDataURL.HowWillBenefit
        }
        else if index == 7 {
            return dictHomeDataURL.contact
//            return dictHomeDataURL.TopicsCovered
        }
        return ""
        
        /*
         if index == 0{
            return dictHomeDataURL.introduction
        }
        else if index == 1{
            return dictHomeDataURL.programDetails
        }else if index == 2{
            return dictHomeDataURL.accomodation
        }else if index == 3{
            return dictHomeDataURL.faculties
        }else if index == 4{
            return dictHomeDataURL.registrationInstruction
        }else if index == 5{
            return dictHomeDataURL.contact
        }else if index == 6 {
//            return dictHomeDataURL.contact
            return dictHomeDataURL.HowWillBenefit
        }else if index == 7 {
//            return dictHomeDataURL.contact
            return dictHomeDataURL.TopicsCovered
        }
        return ""
 */
    }
    
    
}

//MARK:- Service

extension HomeViewController {   
    
    func homeDataAPICalling(completionHandlor:@escaping()->Void) {
        self.view.endEditing(true)
        
        let url = "\(kBaseURL)\(kHomeData)"
        
        let param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : getCurrentTimeZone()]
        
        CommonService().Service(url: url,param: param) { (respones) in
            
            if let json = respones.value
            {
                print("JSON : \(json)")
                
                if json["flag"].stringValue == strSuccessResponse
                {
                    if let data = json.dictionaryObject, let dictURL = HomeDataModel(JSON: data){
                        self.dictHomeDataURL = dictURL.allDataURL!
                        completionHandlor()
                    }
                }
                else if json["flag"].stringValue == strAccessDenied
                {
                   
                }
                else
                {
                    makeToast(strMessage: json["msg"].stringValue)
                }
                
            }
            else
            {
                makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
            }
        }
    }
}

//
//  HomeCollectionCell.swift
//  PGCME
//
//  Created by YASH on 22/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblName.textColor = UIColor.appThemeDarkBlueColor
        lblName.font = themeFont(size: 15, fontname: .bold)
        
        // Initialization code
    }

}

//
//  AppDelegate.swift
//  PGCME
//
//  Created by YASH on 21/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

//Kill every time when user click on Home button
//<key>UIApplicationExitsOnSuspend</key>
//<true/>

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let shared = UIApplication.shared.delegate as! AppDelegate


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //Redirect with sidemenu
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        navigateLGSideEffect(obj: home)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        print("url:- \(url)")
//        http://pgcmehematology.net/admin-panel/send_link
        
        if url.scheme == "https://testflight.apple.com/join/vqg4o8T9" {
            print("work")
        }
        
//        if url.absoluteString == "https://testflight.apple.com/join/vqg4o8T9" {
//            print("true")
//            isDeepLink = true
//            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let home = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//            navigateLGSideEffect(obj: home)
//        }
        
        self.window?.makeKeyAndVisible()
        return true
    }
    
}


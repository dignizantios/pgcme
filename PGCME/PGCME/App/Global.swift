//
//  Global.swift
//  PGCME
//
//  Created by YASH on 22/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation
import MaterialComponents
import SwiftyJSON
import SVProgressHUD

//MARK: - navigate with sidemenu

func navigateLGSideEffect(obj : UIViewController)
{
    let navigationController = NavigationController(rootViewController: obj)
    let mainViewController = MainViewController()
    
    mainViewController.rootViewController = navigationController
    mainViewController.setup(type: 6)
    
    let window = UIApplication.shared.delegate!.window!!
    window.rootViewController = mainViewController
}


//MARK: - Set Toaster
func makeToast(strMessage : String){
    
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager.show(messageSnack)
    
}

//MARK:- Set defaults value

func getUserDetail(_ forKey: String) -> String{
    guard let userDetail = UserDefaults.standard.value(forKey: "userDetails") as? Data else { return "" }
    let data = JSON(userDetail)
    return data[forKey].stringValue
}

//MARK:- Get current timezone

func getCurrentTimeZone() -> String
{
    var localTimeZoneName: String
    {
        return TimeZone.current.identifier
    }
    return localTimeZoneName // "America/Sao_Paulo"
}

//MARK:- Loader

func showLoader() {
    AppDelegate.shared.window?.isUserInteractionEnabled = false
    SVProgressHUD.show()
 
}

func hideLoader(){
    AppDelegate.shared.window?.isUserInteractionEnabled = true
    SVProgressHUD.dismiss()
}


//MARK:- Authentication

var basic_username = "PGCME"
var basic_password = "Rd4AXxJvQgiqP15mQ"
var lang = "0"
var guestPassword = "a25E?6LoYfXUWf>4"
let Defaults = UserDefaults.standard
let strDeviceType = "1"
let strSuccessResponse = "1"
let strAccessDenied = "-1"
var dictSideMenu = JSON()
var ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
var NUMERIC_CHARACTERS = "0123456789+"
var passwordLegth = 6
var mobileLength = 10
var isDeepLink = false


//MARK: - Date
func StringToDate(Formatter : String,strDate : String) -> Date
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = Formatter
    //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
    
    guard let convertedDate = dateformatter.date(from: strDate) else {
        let str = dateformatter.string(from: Date())
        return dateformatter.date(from: str)!
    }
    print("convertedDate - ",convertedDate)
    return convertedDate
}

func DateToString(Formatter : String,date : Date) -> String
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = Formatter
    //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
    let convertedString = dateformatter.string(from: date)
    return convertedString
}



//
//  ServiceListPostfix.swift
//  Hand2Home
//
//  Created by YASH on 03/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation

let kBaseURL = "http://139.59.79.228/PGCME/admin-panel/api/user/"   //Test

//let kBaseURL = "http://pgcmehematology.net/admin-panel/api/user/"   //Live

//let  kBaseURL = "http://demo6.jbsoft.in/pgcme/admin-panel/api/user/"

//let kBaseURL = "http://dignizant.com/PGCME/admin-panel/api/user/"

let kGenerateAccessToken = "generate_access_token"

let kMenuFlag = "all_menu_flag"

let kLogin = "login"

let kGetCourse = "course_data"

let kGetStudyData = "year_of_study_data"

//let kRegister = "register"

let kRegister = "member_register"

let kLogout = "logout"

let kEducationMaterial = "education_material"

let kGetAndUpdateProfileDetails = "profile_update"

let kHomeData = "home_data"

let kChangePassword = "change_password"

let kForgotPassword = "forgot_password"

let kAnnouncementList = "announcements"

let kQuestionariesFlag = "questionnaries_flag"

let kGetFeedbackForm = "feedback_form"

let kGetQuestionariesList = "questionnaries_list"

let kPostQuestionAnswer = "submit_question"

let kPostFeedbackQuestionAnswer = "submit_feedback_answer"

let kPostEmailVerification = "verify_email"

let kGetPayment = "payment_gateway"

let kFeedbackList = "feedback_list"


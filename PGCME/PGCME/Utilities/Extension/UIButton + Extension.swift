//
//  UIButton + Extension.swift
//  Liber
//
//  Created by YASH on 25/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit


extension UIButton
{
    
    func setupThemeButtonUI()
    {
        self.setTitleColor(UIColor.white, for: .normal)
        self.backgroundColor = UIColor.appThemeDarkBlueColor
        self.titleLabel?.font = themeFont(size: 17, fontname: .medium)
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        
    }
    
}

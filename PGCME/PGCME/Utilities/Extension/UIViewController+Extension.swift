//
//  UIViewController+Extension.swift
//  Liber
//
//  Created by om on 10/3/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import AVFoundation
import SwiftyJSON
import AlamofireSwiftyJSON
import Alamofire
import DropDown

extension UIViewController
{
    
    //MARKL - Fonts
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    //MARK: - Images with String
    
    
    func getAttributedString(imgAttachment:UIImage,strPostText:String) -> NSAttributedString {
        let fullString = NSMutableAttributedString(string: "")
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = imgAttachment
        let image1String = NSAttributedString(attachment: image1Attachment)
        fullString.append(image1String)
        fullString.append(NSAttributedString(string: strPostText))
        return fullString
    }
    
    
    func getAttributedString(imgAttachment:UIImage,strPreText:String) -> NSAttributedString {
        let fullString = NSMutableAttributedString(string: "")
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = imgAttachment
        let image1String = NSAttributedString(attachment: image1Attachment)
        fullString.append(NSAttributedString(string: strPreText))
        fullString.append(image1String)
        return fullString
    }
    
    
    //MARK: - Call Method
    
    
    func callUser(strPhoneNumber:String)
    {
        
        let phone = strPhoneNumber
        
        var phoneStr: String = "telprompt://\(phone)"
        phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
        
        let urlPhone = URL(string: phoneStr)
        if UIApplication.shared.canOpenURL(urlPhone!)
        {
            //UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(urlPhone!)
            }
            
        }
        else
        {
            // KSToastView.ks_showToast("Call facility is not available!!!", duration: ToastDuration)
        }
        
    }
    
    //MARK: - navigation with sideMenu
    
    func setupNavigationbarwithSideMenuButton(titleText:String)
    {
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = false
        sideMenuController?.isLeftViewSwipeGestureDisabled = false
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_side_menu"), style: .plain, target: self, action: #selector(sideMenuAction))
        leftButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = leftButton
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = .white
        HeaderView.font = themeFont(size: 18, fontname: .medium)
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.appThemeDarkBlueColor
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeDarkBlueColor
        
        self.navigationItem.titleView = HeaderView
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    @objc func sideMenuAction()
    {
        sideMenuController?.leftViewController?.showLeftViewAnimated(true)
    }
    
    func navigateToSideMenuSelectedController(obj : UIViewController)
    {
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.setViewControllers([obj], animated: false)
        mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
    }
    
    
    
    func setupNavigationbarwithBackButton(titleText:String)
    {
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = false
        sideMenuController?.isLeftViewSwipeGestureDisabled = true
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_white") , style: .plain, target: self, action: #selector(backButtonTapped))
        leftButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = leftButton
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = .white
        HeaderView.font = themeFont(size: 18, fontname: .medium)
        HeaderView.numberOfLines = 0
        HeaderView.textAlignment = .center
        self.navigationItem.titleView = HeaderView
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.appThemeDarkBlueColor
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeDarkBlueColor
    }
    
    @objc func backButtonTapped()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func transperentNavigationBar(){
        
        sideMenuController?.isLeftViewSwipeGestureDisabled = true
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_white") , style: .plain, target: self, action: #selector(backButtonTapped))
        leftButton.tintColor = .black
        self.navigationItem.leftBarButtonItem = leftButton
        
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
    }   
   
    //MARK: - Configure Dropdown
    func configureDropdown(dropdown : DropDown,sender:UIControl,isWidth:Bool = true)
    {
        dropdown.clearSelection()
        dropdown.anchorView = sender
        dropdown.direction = .bottom
        dropdown.dismissMode = .automatic
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        if isWidth{
            dropdown.width = sender.frame.size.width
        } else {
            dropdown.bottomOffset = CGPoint(x: -40, y: sender.bounds.height)
        }
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.cancelAction = { [unowned self] in
            print("Drop down dismissed")
        }
    }
    
    
    //MARK: - Textfield Done button (NumberPad)
    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.appThemeDarkBlueColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done_key".localized, style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction(textfield:)))
        done.tintColor = .white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction(textfield:UITextField)
    {
        self.view.endEditing(true)
    }
    
    
    //MARK: - TableView Error Msg
    
    func showErrorMessageInTableView(strMessage:String) {
    
        let lbl = UILabel()
        lbl.text = strMessage
        lbl.font = themeFont(size: 16, fontname: .medium)
        lbl.textAlignment = NSTextAlignment.center
        lbl.textColor = UIColor.appThemeGrayColor
        lbl.center = UITableView().center
        UITableView().backgroundView = lbl
        
    }
    
}

//MARK: - Service

extension UIViewController{
    
    func guestUserLogin(completion:@escaping()-> ()) {
        
        let url = "\(kBaseURL)\(kGenerateAccessToken)"
        
        print("URL: \(url)")
        
        let param = ["lang" : lang,
                     "username" : basic_username,
                     "password" : guestPassword,
                     "timezone" : getCurrentTimeZone(),
                     "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                     "register_id" :  "",
                     "device_type" : strDeviceType
        ]
        
        print("param :\(param)")
        
        CommonService().Service(url: url, param: param) { (respones) in
            
            if let json = respones.value
            {
                print("JSON : \(json)")
                
                if json["flag"].stringValue == strSuccessResponse
                {
                    guard let rowdata = try? json.rawData() else {return}
                    Defaults.setValue(rowdata, forKey: "userDetails")
                    Defaults.synchronize()
                    completion()
                }
                else if json["flag"].stringValue == strAccessDenied
                {
                    
                }
                else
                {
                    makeToast(strMessage: json["msg"].stringValue)
                }
                
            }
            else
            {
                makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
            }
        }
        
    }
    
    func checkSidemenuFlag(completion:@escaping()-> ()) {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBaseURL)\(kMenuFlag)"
            
            print("URL: \(url)")
            
            let param = ["lang" : lang,
                         "user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "timezone" : getCurrentTimeZone()]
            
            print("param :\(param)")
            
            CommonService().Service(url: url,param: param, isShowLoader:true) { (respones) in
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        dictSideMenu = json["data"]
                        NotificationCenter.default.post(name: .didRefreshSideMenu, object: nil, userInfo: nil)
                        
                        completion()
                    }
                    else if json["flag"].stringValue == strAccessDenied
                    {
                        
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
                }
            }
        }
        else
        {
            makeToast(strMessage: "No_internet_connection_Please_try_again_later_key".localized)
        }
        
    }
    
    
    @objc func logoutAPICalling() {
        
        let url = "\(kBaseURL)\(kLogout)"
        
        print("URL: \(url)")
        
        let param = ["lang" : lang,
                     //                         "timezone" : getCurrentTimeZone(),
            "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
            "user_id" : getUserDetail("user_id")
        ]
        
        print("param :\(param)")
        
        CommonService().Service(url: url, param: param) { (respones) in
            
            if let json = respones.value
            {
                print("JSON : \(json)")
                
                if json["flag"].stringValue == strSuccessResponse
                {
                    
                    Defaults.removeObject(forKey: "userDetails")
                    Defaults.removeObject(forKey: "pre_questionnaries_time")
                    Defaults.removeObject(forKey: "post_questionnaries_time")
                    Defaults.removeObject(forKey: "quize_time")
                    Defaults.removeObject(forKey: "feedback_time")                    
                    Defaults.synchronize()
                    
                    let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                    let obj = storyboardMain.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.navigateToSideMenuSelectedController(obj: obj)
                }
                else if json["flag"].stringValue == strAccessDenied
                {
                    
                }
                else
                {
                    makeToast(strMessage: json["msg"].stringValue)
                }
                
            }
            else
            {
                makeToast(strMessage: "Server_not_responding_Please_try_again_later_key".localized)
            }
        }
    }
    

}

//
//  DateFormatter+Extension.swift
//  PGCME
//
//  Created by Jaydeep on 16/12/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation
import UIKit

enum enumDateFormatter:String {
    case dateFormatter1 = "dd-MM-yyyy"
    case dateFormatter2 = "HH:mm:ss"
    case dateFormatter3 = "HH"
    case dateFormatter4 = "mm"
    case dateFormatter5 = "ss"
}

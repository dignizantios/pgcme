//
//  DatePickerVC.swift
//  USteerTeacher
//
//  Created by om on 16/12/19.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

struct datepickerMode
{
    var pickerMode : UIDatePicker.Mode
    
    init(customPickerMode:UIDatePicker.Mode)
    {
        self.pickerMode = customPickerMode
    }
}

class DatePickerVC: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet var vwButtonHeader : UIView!
    @IBOutlet var btnCancel : UIButton!
    @IBOutlet var btnDone : UIButton!
    @IBOutlet var datePicker : UIDatePicker!

    //MARK: - Variables
    var datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.date)
    var isSetMaximumDate = false
    var isSetMinimumDate = false
    var handlerDateTime:(Date,Int) -> Void = {_,_ in}
    var maximumDate = Date()
    var minimumDate = Date()
    var isSetDate = false
    var setDateValue : Date?
    
    //MARK: - Life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetupUI()
    }
    

}
//MARK: - Setup UI
extension DatePickerVC
{
    func SetupUI()
    {
        [btnDone,btnCancel].forEach({
            $0?.setTitleColor(UIColor.white, for: .normal)
            $0?.titleLabel?.font = themeFont(size: 20, fontname: .medium)
        })
        
        btnDone.setTitle("Done_key".localized, for: .normal)
        btnCancel.setTitle("Cancel_key".localized, for: .normal)
        
        vwButtonHeader.backgroundColor = UIColor.appThemeDarkBlueColor
        
        datePicker.datePickerMode = datePickerCustomMode.pickerMode
        datePicker.date = Date()
        
        if(isSetMaximumDate)
        {
            datePicker.maximumDate = maximumDate
        }
        if(isSetMinimumDate)
        {
            datePicker.minimumDate = minimumDate
        }
        
        if(isSetDate)
        {
            datePicker.setDate(setDateValue ?? Date(), animated: false)
        }
    }
}
//MARK: - Button Action
extension DatePickerVC
{
    @IBAction func btnDoneAction(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
        if datePickerCustomMode.pickerMode == .time {
            handlerDateTime(datePicker.date,1)
        }
        else {
            handlerDateTime(datePicker.date,1)
        }       
    }
    
    @IBAction func btnCancelAction(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
